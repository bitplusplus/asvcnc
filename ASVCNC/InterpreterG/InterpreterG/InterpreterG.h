// InterpreterG.h

#pragma once
#include "Stdafx.h"
#include "rs274ngc.h"
#include "rs274ngc_return.h"
#include "ReturnStatus.h"
#include "FormTools.h"


using namespace System;
using namespace System::Threading;
using namespace std;
using namespace System::Diagnostics;
using namespace System::ComponentModel;
using namespace System::Runtime::InteropServices;
using namespace System::Windows::Forms;
//-----------------------------------------------------------//
extern CANON_TOOL_TABLE _tools[];               
extern int _tool_max;
//-------------------------------------------------------//
namespace InterpreterG {
	//-------------------------------------------------------//
	public delegate  void DelegatePrint(String^);
	public delegate  void DelegateError(String^);
	//-------------------------------------------------------//
	
	public ref class RS274
	{
	public: static event  DelegatePrint^ prinevent;
	public: static event  DelegateError^ evenerrmes;
	//-------------------------------------------------------//
	public:RS274()
		   {	
			
			   
		   }//constr
	//-------------------------------------------------------//
	public:~RS274()
		   {
			   


		   }//distruct
	 //-------------------------------------------------------//
	public: void rs274ngc_getcurrentposition(array<double>^ posit)
	{

				int size = Marshal::SizeOf(posit[0]) * posit->Length;
				IntPtr pnt = Marshal::AllocHGlobal(size);

				*((double*)pnt.ToPointer()) = _setup.current_x;
				*((double*)pnt.ToPointer()+1) = _setup.current_y;
				*((double*)pnt.ToPointer()+2) = _setup.current_z;
				*((double*)pnt.ToPointer()+3) = _setup.AA_current;
				*((double*)pnt.ToPointer() + 4) = _setup.BB_current;
				*((double*)pnt.ToPointer() + 5) = _setup.CC_current;



				Marshal::Copy(pnt, posit, 0, posit->Length);
				Marshal::FreeHGlobal(pnt);
	}
	//-------------------------------------------------------//
	public: void rs274ngc_getcurrenttollslot(array<int>^ tolslot)
	{

				int size = Marshal::SizeOf(tolslot[0]) * tolslot->Length;
				IntPtr pnt = Marshal::AllocHGlobal(size);				

				*((int*)pnt.ToPointer()) = _setup.tool_table_index;
				*((int*)pnt.ToPointer() + 1) = _setup.selected_tool_slot;

				Marshal::Copy(pnt, tolslot, 0, tolslot->Length);
				Marshal::FreeHGlobal(pnt);
	}
	//-------------------------------------------------------//
	public: int rs274ngc_nc_set(int numstr)
		   {
				int index;
				int length;
				char nam[RS274NGC_TEXT_SIZE];
				
				char buf[RS274NGC_TEXT_SIZE];				
				int count = 0;
				fseek(_setup.file_pointer, 0, SEEK_SET);				

				if (count < numstr)
				{
					while (fgets(buf, RS274NGC_TEXT_SIZE, _setup.file_pointer))
					{
						count++;
						if (count >= numstr)break;
					}
					_setup.sequence_number = count;
				}
				else if (numstr <= 0)
				{
					rs274ngc_close();
					rs274ngc_file_name(nam, RS274NGC_TEXT_SIZE);
					rs274ngc_open(nam);

				}//reload

				
				return count;
		   }
	//-------------------------------------------------------//
	public: void rs274ngc_edittoolsV(String^ filenametools)
			{
				FormTools ^ frm = gcnew FormTools();
				frm->Tag = filenametools;
				frm->Show();
			}
	//-------------------------------------------------------//
	public: int rs274ngc_read_tool_fileV( String^ filenametools ) 
		   {
			   FILE * tool_file_port;
			   char buffer[1000];
			   char bufferout[1010];
			   int slot;
			   int tool_id;
			   double offset;
			   double diameter;
			   char * file_name = (char*)StringToConsCh( filenametools);

			   if (file_name[0] IS 0)                        /* ask for name if given name is empty string */
			   {
				   sprintf(bufferout, "name of tool file => ");
				   errwr(bufferout);
				   gets(buffer);
				   tool_file_port SET_TO fopen(buffer, "r");
			   }
			   else
				   tool_file_port SET_TO fopen(file_name, "r");
			   if (tool_file_port IS NULL)
			   {
				   sprintf(bufferout, "Cannot open %s\n", ((file_name[0] IS 0) ? buffer : file_name));
				   errwr(bufferout);
				   return 1;
			   }
			   for(;;)                                       /* read and discard header, checking for blank line */
			   {
				   if (fgets(buffer, 1000, tool_file_port) IS NULL)
				   {
					   sprintf(bufferout, "Bad tool file format\n");
					   errwr(bufferout);
					   return 1;
				   }
				   else if (buffer[0] IS '\n') break;
			   }

			   for (slot SET_TO 0; slot <= _tool_max; slot++)/* initialize */
			   {
				   _tools[slot].id SET_TO -1;
				   _tools[slot].length SET_TO 0;
				   _tools[slot].diameter SET_TO 0;
			   }
			   for (; (fgets(buffer, 1000, tool_file_port) ISNT NULL); )
			   {      
				   if (sscanf(buffer, "%d %d %lf %lf", &slot, &tool_id, &offset, &diameter) < 4)
				   {
					   sprintf(bufferout, "Bad input line \"%s\" in tool file\n", buffer);
					   errwr(bufferout);
					   return 1;
				   }
				   if ((slot < 0) OR (slot > _tool_max))     /* zero and max both OK */
				   {
					   sprintf(bufferout, "Out of range tool slot number %d\n", slot);
					   errwr(bufferout);
					   return 1;
				   }
				   _tools[slot].id SET_TO tool_id;
				   _tools[slot].length SET_TO offset;
				   _tools[slot].diameter SET_TO diameter;
			   }
			   fclose(tool_file_port);
			   return 0;
		   }
	//-------------------------------------------------------//
	public:	void rs274ngc_report_errorV(int error_code, int print_stack)                                 
			{
				char buffer[RS274NGC_TEXT_SIZE];
				char bufferout[RS274NGC_TEXT_SIZE+100];
				int k;

				//rs274ngc_error_text(error_code, buffer, 5);   /* for coverage of code */
				rs274ngc_error_text(error_code, buffer, RS274NGC_TEXT_SIZE);

				if (buffer[0] != 0)	sprintf(bufferout, "%s\n", buffer);
				errwr(bufferout);
				rs274ngc_line_text(buffer, RS274NGC_TEXT_SIZE);
				sprintf(bufferout, "%s\n", buffer);
				errwr(bufferout);
				if (print_stack IS ON)
				{
					for (k SET_TO 0; ; k++)
					{
						rs274ngc_stack_name(k, buffer, RS274NGC_TEXT_SIZE);
						if (buffer[0] ISNT 0)
						{

							sprintf(bufferout, "%s\n", buffer);
							errwr(bufferout);
						}else
						{
							break;
						}
					}
				}
			}
	//-------------------------------------------------------//
	public: static void errwr(char*buf)
			{
				String^ stree = CharToStrin(buf,RS274NGC_TEXT_SIZE);
				evenerrmes(stree);
			}
	//-------------------------------------------------------//
	public: static void prin(char* buf)
			{
				String^ strprint = CharToStrin(buf,1023);
				prinevent(strprint);
			}//
	//-------------------------------------------------------//
	public: static String^ CharToStrin( char* buf, int max ) 
			 {
				 String^ str="";
				 int len = 0;

				 while( (len < max) && (buf[ len ] != '\0') ) 
				 {
					 str+= wchar_t(buf[ len ]);
					 len++;
				 }
				 return str;
			 }
	 //-------------------------------------------------------//
	public: const char* StringToConsCh(String^mes)
			{
				return  (const char*)(Marshal::StringToHGlobalAnsi(mes)).ToPointer();				
				//Marshal::FreeHGlobal(IntPtr((void*)chars));
			}
	 //-------------------------------------------------------//

	public: int rs274ngc_closeV()
			{// close the currently open NC code file
				return rs274ngc_close();
			}
	//-------------------------------------------------------//
	public: int rs274ngc_executeV()
			{//  // execute a line of NC code
				return rs274ngc_execute();
			}
	//-------------------------------------------------------//
	public: int  rs274ngc_exitV()
			{//  // stop running
				return rs274ngc_exit();
			}
	//-------------------------------------------------------//
	public: int rs274ngc_initV()
			{//  // get ready to run
				return  rs274ngc_init();
			}
	//-------------------------------------------------------//
	public: int rs274ngc_load_tool_tableV()
			{//  // load a tool table
				return  rs274ngc_load_tool_table();
			}
	//-------------------------------------------------------//
	public: int rs274ngc_openV(String^ filename)
			{// // open a file of NC code

				return  rs274ngc_open(StringToConsCh(filename));
			}
	//-------------------------------------------------------//
	public: int rs274ngc_readV(String^ strss)
			{// // read the mdi or the next line of the open NC code file
				return  rs274ngc_read(StringToConsCh(strss));
			}
	//-------------------------------------------------------//
	public: int rs274ngc_resetV()
			{//  // reset yourself
				return  rs274ngc_reset();
			}
	//-------------------------------------------------------//
	public: int rs274ngc_restore_parametersV(String^ filename)
			{// // restore interpreter variables from a file
				return  rs274ngc_restore_parameters(StringToConsCh(filename));
			}
	//-------------------------------------------------------//
	public: int rs274ngc_save_parametersV(String^ filename,array<double>^ parametersmanage)
			{//  // save interpreter variables to file

				int size = Marshal::SizeOf(parametersmanage[0]) * parametersmanage->Length;
				IntPtr pnt = Marshal::AllocHGlobal(size);
				Marshal::Copy(parametersmanage, 0,pnt, parametersmanage->Length);
				return  rs274ngc_save_parameters(StringToConsCh(filename), (double*)pnt.ToPointer());

			}
	//-------------------------------------------------------//
	public: int rs274ngc_synchV()
			{//   // synchronize your internal model with the external world
				return  rs274ngc_synch();
			}
	//-------------------------------------------------------//
	public: void rs274ngc_active_g_codesV( array<int>^ codesmanage) 
			{//   // copy active G codes into array [0]..[11]	
				
				int size = Marshal::SizeOf(codesmanage[0]) * codesmanage->Length;
				IntPtr pnt = Marshal::AllocHGlobal(size);
				Marshal::Copy(codesmanage, 0,pnt, codesmanage->Length);
				rs274ngc_active_g_codes((int*)pnt.ToPointer());	
				Marshal::Copy(pnt, codesmanage,0,codesmanage->Length);
				Marshal::FreeHGlobal(pnt);
				
			}
	//-------------------------------------------------------//
	public: void rs274ngc_active_m_codesV( array<int>^ codesmanage)
			{//   // copy active M codes into array [0]..[6]

				int size = Marshal::SizeOf(codesmanage[0]) * codesmanage->Length;
				IntPtr pnt = Marshal::AllocHGlobal(size);
				Marshal::Copy(codesmanage, 0,pnt, codesmanage->Length);
				rs274ngc_active_m_codes((int*)pnt.ToPointer());	
				Marshal::Copy(pnt, codesmanage,0,codesmanage->Length);
				Marshal::FreeHGlobal(pnt);				  
			}
	//-------------------------------------------------------//
	public: void rs274ngc_active_settingsV(array<double>^ settingsmanage)
			{//   // copy active F, S settings into array [0]..[2]

				int size = Marshal::SizeOf(settingsmanage[0]) * settingsmanage->Length;
				IntPtr pnt = Marshal::AllocHGlobal(size);
				Marshal::Copy(settingsmanage, 0,pnt, settingsmanage->Length);
				rs274ngc_active_settings((double*)pnt.ToPointer());	
				Marshal::Copy(pnt, settingsmanage,0,settingsmanage->Length);
				Marshal::FreeHGlobal(pnt);	

			}

	//-------------------------------------------------------//
	public: int rs274ngc_line_lengthV()
			{//   // return the length of the most recently read line
				 return rs274ngc_line_length();
			}
	//-------------------------------------------------------//
	public: int rs274ngc_sequence_numberV()
			{//    // return the current sequence number (how many lines read)
				 return rs274ngc_sequence_number();
			}
	//-------------------------------------------------------//
	public: String^ rs274ngc_file_nameV( int max_size)
			{//  
				// copy the name of the currently open file into the file_name array,
				// but stop at max_size if the name is longer	
				
				IntPtr pnt = Marshal::AllocHGlobal(max_size);
				rs274ngc_file_name((char*)pnt.ToPointer(),max_size);
		
				String^ ret =  Marshal::PtrToStringAnsi(pnt);

				Marshal::FreeHGlobal(pnt);	
				return ret;
			}
	//-------------------------------------------------------//





	};//class
	//--------------------------------------------------------//
	//--------------------------------------------------------//

}//name space
