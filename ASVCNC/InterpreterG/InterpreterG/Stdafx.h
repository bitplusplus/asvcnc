// stdafx.h: ���������� ���� ��� ����������� ��������� ���������� ������
// ��� ���������� ������ ��� ����������� �������, ������� ����� ������������,
// �� �� ����� ����������

#pragma once

#define _CRT_SECURE_NO_WARNINGS

#define _USE_MATH_DEFINES 

#include <stdio.h>                                /* gets, etc. */
#include <stdlib.h>                               /* exit       */
#include <string.h>  
#include <math.h>
#include <ctype.h>
#include <iostream>
#include <vector>



/*
# ************************************************************************

# This Makefile uses five compiler flags. 
# -DAA means the interpreter should be able to handle an A-axis. 
# -DBB means the interpreter should be able to handle a  B-axis. 
# -DCC means the interpreter should be able to handle a  C-axis.
# -DAXIS_ERROR means reading NC code referencing an axis the interpreter
#    cannot handle should cause the interpreter to signal an error.
#    Without this flag, if the interpreter cannot handle an axis, any
#    syntactially valid NC code word starting with the axis letter is
#    read and ignored.
# -DALL_AXES means the interpreter should print canonical commands
#    that include all six axes, regardless of whether the interpreter
#    can handle those axes. Values put into the canonical command calls
#    for non-handled axes are always zero.

# This Makefile includes code for making six executables out of a-c -v -g
# possible 29. The others can be made similarly.  --g -Wall
*/


#define AA
#define BB
#define CC



