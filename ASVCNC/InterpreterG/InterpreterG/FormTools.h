#pragma once
#include <msclr/marshal.h>
namespace InterpreterG {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Threading;
	using namespace std;
	using namespace System::Diagnostics;
	using namespace System::Runtime::InteropServices;


	/// <summary>
	/// ������ ��� FormTools
	/// </summary>
	public ref class FormTools : public System::Windows::Forms::Form
	{
	public:
		FormTools(void)
		{
			InitializeComponent();
			//
			//TODO: �������� ��� ������������
			//
		}

	protected:
		/// <summary>
		/// ���������� ��� ������������ �������.
		/// </summary>
		~FormTools()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::StatusStrip^  statusStriptools;
	protected: 
	private: System::Windows::Forms::ToolStrip^  toolStriptools;
	private: System::Windows::Forms::Panel^  paneltools;
	private: System::Windows::Forms::DataGridView^  dataGridViewtools;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  toolslot;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  numtools;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  tooloffset;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  tooldiam;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  toolsholders;
	private: System::Windows::Forms::DataGridViewTextBoxColumn^  toolsdeskript;
	private: System::Windows::Forms::ToolStripButton^  toolStripButtonadd;
	private: System::Windows::Forms::ToolStripButton^  toolStripButtonsave;
	private: System::Windows::Forms::ContextMenuStrip^  contextMenuStripDatagird;
	private: System::Windows::Forms::ContextMenuStrip^  contextMenuStripdel;
	private: System::Windows::Forms::ToolStripMenuItem^  �����������������ToolStripMenuItem;


	private: System::ComponentModel::IContainer^  components;











	private:
		/// <summary>
		/// ��������� ���������� ������������.
		/// </summary>


#pragma region Windows Form Designer generated code
		/// <summary>
		/// ������������ ����� ��� ��������� ������������ - �� ���������
		/// ���������� ������� ������ ��� ������ ��������� ����.
		/// </summary>
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(FormTools::typeid));
			this->statusStriptools = (gcnew System::Windows::Forms::StatusStrip());
			this->toolStriptools = (gcnew System::Windows::Forms::ToolStrip());
			this->toolStripButtonadd = (gcnew System::Windows::Forms::ToolStripButton());
			this->toolStripButtonsave = (gcnew System::Windows::Forms::ToolStripButton());
			this->paneltools = (gcnew System::Windows::Forms::Panel());
			this->dataGridViewtools = (gcnew System::Windows::Forms::DataGridView());
			this->toolslot = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->numtools = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->tooloffset = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->tooldiam = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->toolsholders = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->toolsdeskript = (gcnew System::Windows::Forms::DataGridViewTextBoxColumn());
			this->contextMenuStripdel = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
			this->�����������������ToolStripMenuItem = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->contextMenuStripDatagird = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
			this->toolStriptools->SuspendLayout();
			this->paneltools->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridViewtools))->BeginInit();
			this->contextMenuStripdel->SuspendLayout();
			this->SuspendLayout();
			// 
			// statusStriptools
			// 
			this->statusStriptools->Location = System::Drawing::Point(0, 239);
			this->statusStriptools->Name = L"statusStriptools";
			this->statusStriptools->Size = System::Drawing::Size(714, 22);
			this->statusStriptools->TabIndex = 0;
			this->statusStriptools->Text = L"������";
			// 
			// toolStriptools
			// 
			this->toolStriptools->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(2) {
				this->toolStripButtonadd,
					this->toolStripButtonsave
			});
			this->toolStriptools->Location = System::Drawing::Point(0, 0);
			this->toolStriptools->Name = L"toolStriptools";
			this->toolStriptools->Size = System::Drawing::Size(714, 55);
			this->toolStriptools->TabIndex = 1;
			this->toolStriptools->Text = L"������ ����������";
			// 
			// toolStripButtonadd
			// 
			this->toolStripButtonadd->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->toolStripButtonadd->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripButtonadd.Image")));
			this->toolStripButtonadd->ImageAlign = System::Drawing::ContentAlignment::BottomCenter;
			this->toolStripButtonadd->ImageScaling = System::Windows::Forms::ToolStripItemImageScaling::None;
			this->toolStripButtonadd->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButtonadd->Name = L"toolStripButtonadd";
			this->toolStripButtonadd->Size = System::Drawing::Size(52, 52);
			this->toolStripButtonadd->Text = L"�������� ����������";
			this->toolStripButtonadd->Click += gcnew System::EventHandler(this, &FormTools::toolStripButtonadd_Click);
			// 
			// toolStripButtonsave
			// 
			this->toolStripButtonsave->DisplayStyle = System::Windows::Forms::ToolStripItemDisplayStyle::Image;
			this->toolStripButtonsave->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"toolStripButtonsave.Image")));
			this->toolStripButtonsave->ImageScaling = System::Windows::Forms::ToolStripItemImageScaling::None;
			this->toolStripButtonsave->ImageTransparentColor = System::Drawing::Color::Magenta;
			this->toolStripButtonsave->Margin = System::Windows::Forms::Padding(10, 1, 0, 2);
			this->toolStripButtonsave->Name = L"toolStripButtonsave";
			this->toolStripButtonsave->Size = System::Drawing::Size(52, 52);
			this->toolStripButtonsave->Text = L"���������";
			this->toolStripButtonsave->Click += gcnew System::EventHandler(this, &FormTools::toolStripButtonsave_Click);
			// 
			// paneltools
			// 
			this->paneltools->Controls->Add(this->dataGridViewtools);
			this->paneltools->Dock = System::Windows::Forms::DockStyle::Fill;
			this->paneltools->Location = System::Drawing::Point(0, 55);
			this->paneltools->Name = L"paneltools";
			this->paneltools->Size = System::Drawing::Size(714, 184);
			this->paneltools->TabIndex = 2;
			// 
			// dataGridViewtools
			// 
			this->dataGridViewtools->AllowUserToAddRows = false;
			this->dataGridViewtools->AllowUserToDeleteRows = false;
			this->dataGridViewtools->ColumnHeadersHeightSizeMode = System::Windows::Forms::DataGridViewColumnHeadersHeightSizeMode::AutoSize;
			this->dataGridViewtools->Columns->AddRange(gcnew cli::array< System::Windows::Forms::DataGridViewColumn^  >(6) {
				this->toolslot,
					this->numtools, this->tooloffset, this->tooldiam, this->toolsholders, this->toolsdeskript
			});
			this->dataGridViewtools->ContextMenuStrip = this->contextMenuStripdel;
			this->dataGridViewtools->Dock = System::Windows::Forms::DockStyle::Fill;
			this->dataGridViewtools->Location = System::Drawing::Point(0, 0);
			this->dataGridViewtools->Name = L"dataGridViewtools";
			this->dataGridViewtools->RowHeadersWidth = 10;
			this->dataGridViewtools->SelectionMode = System::Windows::Forms::DataGridViewSelectionMode::FullRowSelect;
			this->dataGridViewtools->Size = System::Drawing::Size(714, 184);
			this->dataGridViewtools->TabIndex = 0;
			this->dataGridViewtools->CellContentClick += gcnew System::Windows::Forms::DataGridViewCellEventHandler(this, &FormTools::dataGridViewtools_CellContentClick);
			this->dataGridViewtools->CellMouseDown += gcnew System::Windows::Forms::DataGridViewCellMouseEventHandler(this, &FormTools::dataGridViewtools_CellMouseDown);
			this->dataGridViewtools->CellValidating += gcnew System::Windows::Forms::DataGridViewCellValidatingEventHandler(this, &FormTools::dataGridViewtools_CellValidating);
			// 
			// toolslot
			// 
			this->toolslot->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
			this->toolslot->HeaderText = L"����";
			this->toolslot->MaxInputLength = 20;
			this->toolslot->Name = L"toolslot";
			this->toolslot->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
			// 
			// numtools
			// 
			this->numtools->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
			this->numtools->HeaderText = L"ID �����.";
			this->numtools->MaxInputLength = 20;
			this->numtools->Name = L"numtools";
			this->numtools->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
			// 
			// tooloffset
			// 
			this->tooloffset->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
			this->tooloffset->HeaderText = L"��������";
			this->tooloffset->MaxInputLength = 20;
			this->tooloffset->Name = L"tooloffset";
			this->tooloffset->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
			// 
			// tooldiam
			// 
			this->tooldiam->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
			this->tooldiam->HeaderText = L"�������";
			this->tooldiam->MaxInputLength = 20;
			this->tooldiam->Name = L"tooldiam";
			this->tooldiam->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
			// 
			// toolsholders
			// 
			this->toolsholders->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
			this->toolsholders->HeaderText = L"���������";
			this->toolsholders->MaxInputLength = 20;
			this->toolsholders->Name = L"toolsholders";
			this->toolsholders->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
			// 
			// toolsdeskript
			// 
			this->toolsdeskript->AutoSizeMode = System::Windows::Forms::DataGridViewAutoSizeColumnMode::Fill;
			this->toolsdeskript->HeaderText = L"��������";
			this->toolsdeskript->MaxInputLength = 100;
			this->toolsdeskript->Name = L"toolsdeskript";
			this->toolsdeskript->SortMode = System::Windows::Forms::DataGridViewColumnSortMode::NotSortable;
			// 
			// contextMenuStripdel
			// 
			this->contextMenuStripdel->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(1) { this->�����������������ToolStripMenuItem });
			this->contextMenuStripdel->Name = L"contextMenuStripdel";
			this->contextMenuStripdel->Size = System::Drawing::Size(187, 26);
			// 
			// �����������������ToolStripMenuItem
			// 
			this->�����������������ToolStripMenuItem->Name = L"�����������������ToolStripMenuItem";
			this->�����������������ToolStripMenuItem->Size = System::Drawing::Size(186, 22);
			this->�����������������ToolStripMenuItem->Text = L"������� ����������";
			this->�����������������ToolStripMenuItem->Click += gcnew System::EventHandler(this, &FormTools::�����������������ToolStripMenuItem_Click);
			// 
			// contextMenuStripDatagird
			// 
			this->contextMenuStripDatagird->Name = L"contextMenuStripDatagird";
			this->contextMenuStripDatagird->Size = System::Drawing::Size(61, 4);
			// 
			// FormTools
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(714, 261);
			this->Controls->Add(this->paneltools);
			this->Controls->Add(this->toolStriptools);
			this->Controls->Add(this->statusStriptools);
			this->Name = L"FormTools";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"����� ������������";
			this->TopMost = true;
			this->Load += gcnew System::EventHandler(this, &FormTools::FormTools_Load);
			this->toolStriptools->ResumeLayout(false);
			this->toolStriptools->PerformLayout();
			this->paneltools->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dataGridViewtools))->EndInit();
			this->contextMenuStripdel->ResumeLayout(false);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void dataGridViewtools_CellContentClick(System::Object^  sender, System::Windows::Forms::DataGridViewCellEventArgs^  e)
			 {


			 }
			 //-------------------------------------------------------//
	private: System::Void FormTools_Load(System::Object^  sender, System::EventArgs^  e) 
			 {

				 loadtotablefromfile();


				
			 }//
		 	//-------------------------------------------------------//
	public: static String^ CharToStrin( char* buf, int max ) 
			 {
				 String^ str="";
				 int len = 0;

				 while( (len < max) && (buf[ len ] != '\0') ) 
				 {
					 str+= wchar_t(buf[ len ]);
					 len++;
				 }
				 return str;
			}
			//-------------------------------------------------------//
	private: System::Void dataGridViewtools_CellValidating(System::Object^  sender, System::Windows::Forms::DataGridViewCellValidatingEventArgs^  e)			 
			 {
				 try
				 {
					 if(e->ColumnIndex < 5) Convert::ToDouble(e->FormattedValue->ToString());
				 }
				 catch(FormatException ^ ex)
				 {
					 MessageBox::Show("�������� �������� ������, ������ ���� �����", "������ ����� ");
					 e->Cancel = true;
				 }
			 }
		 //-------------------------------------------------------//
private: System::Void dataGridViewtools_CellMouseDown(System::Object^  sender, System::Windows::Forms::DataGridViewCellMouseEventArgs^  e) 
		 {
			 /*

			 if(e->Button == MouseButtons->Right)
			 {
				 dataGridViewtools->ContextMenuStrip->Show();
			 }
			 */
		 }
		 //-------------------------------------------------------//

private: System::Void �����������������ToolStripMenuItem_Click(System::Object^  sender, System::EventArgs^  e) 
		{
			 if (dataGridViewtools->RowCount >0)
			 deletetoolfromfile(dataGridViewtools->CurrentRow->Index);

		}
		 //-------------------------------------------------------//
		 private: void loadtotablefromfile()
		 {

					  String^ filenametools = (String^)this->Tag;
					  char * file_name = (char*)(Marshal::StringToHGlobalAnsi(filenametools)).ToPointer();
					  FILE * tool_file_port;
					  char buffer[1000];

					  int indextool = 0;
					  int slot;
					  int tool_id;
					  double offset;
					  double diameter;
					  double holder;
					  char deskript[1000];
					  extern int _tool_max;

					  tool_file_port = fopen(file_name, "r");
					  if (tool_file_port == NULL)
					  {
						  MessageBox::Show("�� ������� ������� ���� " + filenametools, "������ ");
					  }
					  else
					  {

						  for (;;)
						  {
							  /* read and discard header, checking for blank line */
							  if (fgets(buffer, 1000, tool_file_port) == NULL)
							  {
								  MessageBox::Show("���� ��������� " + filenametools, "������ ");

							  }
							  else if (buffer[0] == '\n') break;
						  }//for

						  dataGridViewtools->Rows->Clear();
						  indextool = 0;
						  for (; (fgets(buffer, 1000, tool_file_port) != NULL);)
						  {							  

							  if (sscanf(buffer, "%d %d %lf %lf %lf %s", &slot, &tool_id, &offset, &diameter, &holder, &deskript[0]) < 4)
							  {
								  MessageBox::Show("���� ��������� " + filenametools, "������ ");
							  }

							  if ((slot < 0) || (slot > _tool_max))     /* zero and max both OK */
							  {
								  MessageBox::Show("���������� ������������ ��� ��������� " + filenametools, "������ ");

							  }

							  int len = strlen(deskript);

							  dataGridViewtools->Rows->Add();

							  dataGridViewtools->Rows[indextool]->Cells[0]->Value = Convert::ToString(slot);
							  dataGridViewtools->Rows[indextool]->Cells[1]->Value = Convert::ToString(tool_id);
							  dataGridViewtools->Rows[indextool]->Cells[2]->Value = Convert::ToString(offset);
							  dataGridViewtools->Rows[indextool]->Cells[3]->Value = Convert::ToString(diameter);
							  dataGridViewtools->Rows[indextool]->Cells[4]->Value = Convert::ToString(holder);
							  dataGridViewtools->Rows[indextool]->Cells[5]->Value = gcnew  String(deskript, 0, len);



							  indextool++;
						  }

						  fclose(tool_file_port);
					  }//

		 }
		 //-------------------------------------------------------//
				  private: void deletetoolfromfile(int num)
				  {

							   String^ filenametools = (String^)this->Tag;
							   char * file_name = (char*)(Marshal::StringToHGlobalAnsi(filenametools)).ToPointer();
							   FILE * tool_file_port;
							   char buffer[1000];
							   vector<string> lines;
							   vector<string>::const_iterator i;

							   int indextool = 0;
							   int slot;
							   int tool_id;
							   double offset;
							   double diameter;
							   double holder;
							   char deskript[1000];
							   extern int _tool_max;

							   tool_file_port = fopen(file_name, "r");
							   if (tool_file_port == NULL)
							   {
								   MessageBox::Show("�� ������� ������� ���� " + filenametools, "������ ");
							   }
							   else
							   {

								   for (;;)
								   {
									   /* read and discard header, checking for blank line */
									   if (fgets(buffer, 1000, tool_file_port) == NULL)
									   {
										   MessageBox::Show("���� ��������� " + filenametools, "������ ");

									   }
									   else{
										   lines.push_back(buffer);
										   if (buffer[0] == '\n') break;
									   }									   
									    
								   }//for
								  
								   indextool = 0;
								   for (; (fgets(buffer, 1000, tool_file_port) != NULL);)
								   {

									   if (sscanf(buffer, "%d %d %lf %lf %lf %s", &slot, &tool_id, &offset, &diameter, &holder, &deskript[0]) < 4)
									   {
										   MessageBox::Show("���� ��������� " + filenametools, "������ ");
									   }

									   if ((slot < 0) || (slot > _tool_max))     /* zero and max both OK */
									   {
										   MessageBox::Show("���������� ������������ ��� ��������� " + filenametools, "������ ");

									   }

									   if (num != indextool)
									   {
										   lines.push_back(buffer);
									   }

									   indextool++;
								   }

								   fclose(tool_file_port);


							   }//

							   tool_file_port = fopen(file_name, "w");							  
							   for (i = lines.begin(); i != lines.end(); ++i)
								   fputs( (*i).c_str(), tool_file_port);
							   fclose(tool_file_port);

							   loadtotablefromfile();

				  }
				 //-------------------------------------------------------//
				private: System::Void toolStripButtonadd_Click(System::Object^  sender, System::EventArgs^  e) 
						   {

							 dataGridViewtools->Rows->Add();

							 dataGridViewtools->Rows[dataGridViewtools->RowCount-1]->Cells[0]->Value = Convert::ToString(1);
							 dataGridViewtools->Rows[dataGridViewtools->RowCount - 1]->Cells[1]->Value = Convert::ToString(1);
							 dataGridViewtools->Rows[dataGridViewtools->RowCount - 1]->Cells[2]->Value = Convert::ToString(2);
							 dataGridViewtools->Rows[dataGridViewtools->RowCount - 1]->Cells[3]->Value = Convert::ToString(3);
							 dataGridViewtools->Rows[dataGridViewtools->RowCount - 1]->Cells[4]->Value = Convert::ToString(0);
							 dataGridViewtools->Rows[dataGridViewtools->RowCount - 1]->Cells[5]->Value = "����� �������� 3��";


						   }
				//-------------------------------------------------------//
				private: System::Void toolStripButtonsave_Click(System::Object^  sender, System::EventArgs^  e) 
				{
	 

							 String^ filenametools = (String^)this->Tag;
							 char * file_name = (char*)(Marshal::StringToHGlobalAnsi(filenametools)).ToPointer();
							 FILE * tool_file_port;
							 char buffer[1000];
							 vector<string> lines;
							 vector<string>::const_iterator i;						 

							
							 extern int _tool_max;

							 tool_file_port = fopen(file_name, "r");
							 if (tool_file_port == NULL)
							 {
								 MessageBox::Show("�� ������� ������� ���� " + filenametools, "������ ");
							 }
							 else
							 {

								 for (;;)
								 {
									 /* read and discard header, checking for blank line */
									 if (fgets(buffer, 1000, tool_file_port) == NULL)
									 {
										 MessageBox::Show("���� ��������� " + filenametools, "������ ");

									 }
									 else{
										 lines.push_back(buffer);
										 if (buffer[0] == '\n') break;
									 }

								 }//for

								 for (int i = 0; i < dataGridViewtools->RowCount; i++)
								 {
									 int slot = Convert::ToInt32( dataGridViewtools->Rows[i]->Cells[0]->Value);
									 int tollid = Convert::ToInt32(dataGridViewtools->Rows[i]->Cells[1]->Value);
									 float offset = (float)Convert::ToDouble(dataGridViewtools->Rows[i]->Cells[2]->Value);
									 float diametr = (float)Convert::ToDouble(dataGridViewtools->Rows[i]->Cells[3]->Value);
									 float holder =(float) Convert::ToDouble(dataGridViewtools->Rows[i]->Cells[4]->Value);
									 String^ strdes = dataGridViewtools->Rows[i]->Cells[5]->Value->ToString();
									 strdes += "\n";

									 msclr::interop::marshal_context oMarshalContext;
									 sprintf(buffer, "%d %d %lf %lf %lf %s", slot, tollid, offset, diametr, holder, (const char*)oMarshalContext.marshal_as<const char*>(strdes));
									 lines.push_back(buffer);

								 }
								 fclose(tool_file_port);


							 }//						



							 tool_file_port = fopen(file_name, "w");
							 for (i = lines.begin(); i != lines.end(); ++i)
								 fputs((*i).c_str(), tool_file_port);
							 fclose(tool_file_port);

							 loadtotablefromfile();

							 


				}
				//-------------------------------------------------------//





};//end class
}//end space




