#pragma once

namespace InterpreterG {
public ref class ReturnStatus
{
public: ReturnStatus(void);

public: static const int sRS274NGC_OK= 0;
public: static const int sRS274NGC_EXIT= 1;
public: static const int sRS274NGC_EXECUTE_FINISH= 2;
public: static const int sRS274NGC_ENDFILE= 3;
public: static const int sNCE_A_FILE_IS_ALREADY_OPEN =4;
public: static const int sNCE_ALL_AXES_MISSING_WITH_G92= 5;
public: static const int sNCE_ALL_AXES_MISSING_WITH_MOTION_CODE= 6;
public: static const int sNCE_ARC_RADIUS_TOO_SMALL_TO_REACH_END_POINT =7;
public: static const int sNCE_ARGUMENT_TO_ACOS_OUT_OF_RANGE =8;
public: static const int sNCE_ARGUMENT_TO_ASIN_OUT_OF_RANGE= 9;
public: static const int sNCE_ATTEMPT_TO_DIVIDE_BY_ZERO =10;
public: static const int sNCE_ATTEMPT_TO_RAISE_NEGATIVE_TO_NON_INTEGER_POWER =11;
public: static const int sNCE_BAD_CHARACTER_USED =12;
public: static const int sNCE_BAD_FORMAT_UNSIGNED_INTEGER= 13;
public: static const int sNCE_BAD_NUMBER_FORMAT= 14;
public: static const int sNCE_BUG_BAD_G_CODE_MODAL_GROUP_0 =15;
public: static const int sNCE_BUG_CODE_NOT_G0_OR_G1 =16;
public: static const int sNCE_BUG_CODE_NOT_G17_G18_OR_G19 =17;
public: static const int sNCE_BUG_CODE_NOT_G20_OR_G21 =18;
public: static const int sNCE_BUG_CODE_NOT_G28_OR_G30 =19;
public: static const int sNCE_BUG_CODE_NOT_G2_OR_G3= 20;
public: static const int sNCE_BUG_CODE_NOT_G40_G41_OR_G42= 21;
public: static const int sNCE_BUG_CODE_NOT_G43_OR_G49 =22;
public: static const int sNCE_BUG_CODE_NOT_G4_G10_G28_G30_G53_OR_G92_SERIES =23;
public: static const int sNCE_BUG_CODE_NOT_G61_G61_1_OR_G64 =24;
public: static const int sNCE_BUG_CODE_NOT_G90_OR_G91 =25;
public: static const int sNCE_BUG_CODE_NOT_G93_OR_G94 =26;
public: static const int sNCE_BUG_CODE_NOT_G98_OR_G99 =27;
public: static const int sNCE_BUG_CODE_NOT_IN_G92_SERIES =28;
public: static const int sNCE_BUG_CODE_NOT_IN_RANGE_G54_TO_G593= 29;
public: static const int sNCE_BUG_CODE_NOT_M0_M1_M2_M30_M60 =30;
public: static const int sNCE_BUG_DISTANCE_MODE_NOT_G90_OR_G91 =31;
public: static const int sNCE_BUG_FUNCTION_SHOULD_NOT_HAVE_BEEN_CALLED =32;
public: static const int sNCE_BUG_IN_TOOL_RADIUS_COMP= 33;
public: static const int sNCE_BUG_PLANE_NOT_XY_YZ_OR_XZ =34;
public: static const int sNCE_BUG_SIDE_NOT_RIGHT_OR_LEFT= 35;
public: static const int sNCE_BUG_UNKNOWN_MOTION_CODE= 36;
public: static const int sNCE_BUG_UNKNOWN_OPERATION =37;
public: static const int sNCE_CANNOT_CHANGE_AXIS_OFFSETS_WITH_CUTTER_RADIUS_COMP =38;
public: static const int sNCE_CANNOT_CHANGE_UNITS_WITH_CUTTER_RADIUS_COMP =39;
public: static const int sNCE_CANNOT_CREATE_BACKUP_FILE =40;
public: static const int sNCE_CANNOT_DO_G1_WITH_ZERO_FEED_RATE =41;
public: static const int sNCE_CANNOT_DO_ZERO_REPEATS_OF_CYCLE= 42;
public: static const int sNCE_CANNOT_MAKE_ARC_WITH_ZERO_FEED_RATE =43;
public: static const int sNCE_CANNOT_MOVE_ROTARY_AXES_DURING_PROBING =44;
public: static const int sNCE_CANNOT_OPEN_BACKUP_FILE= 45;
public: static const int sNCE_CANNOT_OPEN_VARIABLE_FILE =46;
public: static const int sNCE_CANNOT_PROBE_IN_INVERSE_TIME_FEED_MODE =47;
public: static const int sNCE_CANNOT_PROBE_WITH_CUTTER_RADIUS_COMP_ON =48;
public: static const int sNCE_CANNOT_PROBE_WITH_ZERO_FEED_RATE =49;
public: static const int sNCE_CANNOT_PUT_A_B_IN_CANNED_CYCLE =50;
public: static const int sNCE_CANNOT_PUT_A_C_IN_CANNED_CYCLE =51;
public: static const int sNCE_CANNOT_PUT_AN_A_IN_CANNED_CYCLE =52;
public: static const int sNCE_CANNOT_TURN_CUTTER_RADIUS_COMP_ON_OUT_OF_XY_PLANE =53;
public: static const int sNCE_CANNOT_TURN_CUTTER_RADIUS_COMP_ON_WHEN_ON =54;
public: static const int sNCE_CANNOT_USE_A_WORD =55;
public: static const int sNCE_CANNOT_USE_AXIS_VALUES_WITH_G80 =56;
public: static const int sNCE_CANNOT_USE_AXIS_VALUES_WITHOUT_A_G_CODE_THAT_USES_THEM =57;
public: static const int sNCE_CANNOT_USE_B_WORD= 58;
public: static const int sNCE_CANNOT_USE_C_WORD =59;
public: static const int sNCE_CANNOT_USE_G28_OR_G30_WITH_CUTTER_RADIUS_COMP= 60;
public: static const int sNCE_CANNOT_USE_G53_INCREMENTAL= 61;
public: static const int sNCE_CANNOT_USE_G53_WITH_CUTTER_RADIUS_COMP =62;
public: static const int sNCE_CANNOT_USE_TWO_G_CODES_THAT_BOTH_USE_AXIS_VALUES =63;
public: static const int sNCE_CANNOT_USE_XZ_PLANE_WITH_CUTTER_RADIUS_COMP= 64;
public: static const int sNCE_CANNOT_USE_YZ_PLANE_WITH_CUTTER_RADIUS_COMP =65;
public: static const int sNCE_COMMAND_TOO_LONG= 66;
public: static const int sNCE_CONCAVE_CORNER_WITH_CUTTER_RADIUS_COMP= 67;
public: static const int sNCE_COORDINATE_SYSTEM_INDEX_PARAMETER_5220_OUT_OF_RANGE =68;
public: static const int sNCE_CURRENT_POINT_SAME_AS_END_POINT_OF_ARC =69;
public: static const int sNCE_CUTTER_GOUGING_WITH_CUTTER_RADIUS_COMP =70;
public: static const int sNCE_D_WORD_WITH_NO_G41_OR_G42 =71;
public: static const int sNCE_DWELL_TIME_MISSING_WITH_G4 =72;
public: static const int sNCE_DWELL_TIME_P_WORD_MISSING_WITH_G82= 73;
public: static const int sNCE_DWELL_TIME_P_WORD_MISSING_WITH_G86 =74;
public: static const int sNCE_DWELL_TIME_P_WORD_MISSING_WITH_G88 =75;
public: static const int sNCE_DWELL_TIME_P_WORD_MISSING_WITH_G89 =76;
public: static const int sNCE_EQUAL_SIGN_MISSING_IN_PARAMETER_SETTING =77;
public: static const int sNCE_F_WORD_MISSING_WITH_INVERSE_TIME_ARC_MOVE =78;
public: static const int sNCE_F_WORD_MISSING_WITH_INVERSE_TIME_G1_MOVE =79;
public: static const int sNCE_FILE_ENDED_WITH_NO_PERCENT_SIGN =80;
public: static const int sNCE_FILE_ENDED_WITH_NO_PERCENT_SIGN_OR_PROGRAM_END =81;
public: static const int sNCE_FILE_NAME_TOO_LONG =82;
public: static const int sNCE_FILE_NOT_OPEN =83;
public: static const int sNCE_G_CODE_OUT_OF_RANGE =84;
public: static const int sNCE_H_WORD_WITH_NO_G43= 85;
public: static const int sNCE_I_WORD_GIVEN_FOR_ARC_IN_YZ_PLANE =86;
public: static const int sNCE_I_WORD_MISSING_WITH_G87 =87;
public: static const int sNCE_I_WORD_WITH_NO_G2_OR_G3_OR_G87_TO_USE_IT =88;
public: static const int sNCE_J_WORD_GIVEN_FOR_ARC_IN_XZ_PLANE =89;
public: static const int sNCE_J_WORD_MISSING_WITH_G87 =90;
public: static const int sNCE_J_WORD_WITH_NO_G2_OR_G3_OR_G87_TO_USE_IT= 91;
public: static const int sNCE_K_WORD_GIVEN_FOR_ARC_IN_XY_PLANE= 92;
public: static const int sNCE_K_WORD_MISSING_WITH_G87 =93;
public: static const int sNCE_K_WORD_WITH_NO_G2_OR_G3_OR_G87_TO_USE_IT =94;
public: static const int sNCE_L_WORD_WITH_NO_CANNED_CYCLE_OR_G10= 95;
public: static const int sNCE_LEFT_BRACKET_MISSING_AFTER_SLASH_WITH_ATAN =96;
public: static const int sNCE_LEFT_BRACKET_MISSING_AFTER_UNARY_OPERATION_NAME= 97;
public: static const int sNCE_LINE_NUMBER_GREATER_THAN_99999 =98;
public: static const int sNCE_LINE_WITH_G10_DOES_NOT_HAVE_L2 =99;
public: static const int sNCE_M_CODE_GREATER_THAN_99= 100;
public: static const int sNCE_MIXED_RADIUS_IJK_FORMAT_FOR_ARC =101;
public: static const int sNCE_MULTIPLE_A_WORDS_ON_ONE_LINE =102;
public: static const int sNCE_MULTIPLE_B_WORDS_ON_ONE_LINE =103;
public: static const int sNCE_MULTIPLE_C_WORDS_ON_ONE_LINE =104;
public: static const int sNCE_MULTIPLE_D_WORDS_ON_ONE_LINE =105;
public: static const int sNCE_MULTIPLE_F_WORDS_ON_ONE_LINE= 106;
public: static const int sNCE_MULTIPLE_H_WORDS_ON_ONE_LINE= 107;
public: static const int sNCE_MULTIPLE_I_WORDS_ON_ONE_LINE= 108;
public: static const int sNCE_MULTIPLE_J_WORDS_ON_ONE_LINE =109;
public: static const int sNCE_MULTIPLE_K_WORDS_ON_ONE_LINE =110;
public: static const int sNCE_MULTIPLE_L_WORDS_ON_ONE_LINE =111;
public: static const int sNCE_MULTIPLE_P_WORDS_ON_ONE_LINE =112;
public: static const int sNCE_MULTIPLE_Q_WORDS_ON_ONE_LINE =113;
public: static const int sNCE_MULTIPLE_R_WORDS_ON_ONE_LINE =114;
public: static const int sNCE_MULTIPLE_S_WORDS_ON_ONE_LINE =115;
public: static const int sNCE_MULTIPLE_T_WORDS_ON_ONE_LINE =116;
public: static const int sNCE_MULTIPLE_X_WORDS_ON_ONE_LINE =117;
public: static const int sNCE_MULTIPLE_Y_WORDS_ON_ONE_LINE =118;
public: static const int sNCE_MULTIPLE_Z_WORDS_ON_ONE_LINE =119;
public: static const int sNCE_MUST_USE_G0_OR_G1_WITH_G53 =120;
public: static const int sNCE_NEGATIVE_ARGUMENT_TO_SQRT= 121;
public: static const int sNCE_NEGATIVE_D_WORD_TOOL_RADIUS_INDEX_USED =122;
public: static const int sNCE_NEGATIVE_F_WORD_USED =123;
public: static const int sNCE_NEGATIVE_G_CODE_USED =124;
public: static const int sNCE_NEGATIVE_H_WORD_TOOL_LENGTH_OFFSET_INDEX_USED =125;
public: static const int sNCE_NEGATIVE_L_WORD_USED =126;
public: static const int sNCE_NEGATIVE_M_CODE_USED =127;
public: static const int sNCE_NEGATIVE_OR_ZERO_Q_VALUE_USED =128;
public: static const int sNCE_NEGATIVE_P_WORD_USED =129;
public: static const int sNCE_NEGATIVE_SPINDLE_SPEED_USED =130;
public: static const int sNCE_NEGATIVE_TOOL_ID_USED =131;
public: static const int sNCE_NESTED_COMMENT_FOUND =132;
public: static const int sNCE_NO_CHARACTERS_FOUND_IN_READING_REAL_VALUE =133;
public: static const int sNCE_NO_DIGITS_FOUND_WHERE_REAL_NUMBER_SHOULD_BE =134;
public: static const int sNCE_NON_INTEGER_VALUE_FOR_INTEGER= 135;
public: static const int sNCE_NULL_MISSING_AFTER_NEWLINE =136;
public: static const int sNCE_OFFSET_INDEX_MISSING =137;
public: static const int sNCE_P_VALUE_NOT_AN_INTEGER_WITH_G10_L2 =138;
public: static const int sNCE_P_VALUE_OUT_OF_RANGE_WITH_G10_L2 =139;
public: static const int sNCE_P_WORD_WITH_NO_G4_G10_G82_G86_G88_G89 =140;
public: static const int sNCE_PARAMETER_FILE_OUT_OF_ORDER =141;
public: static const int sNCE_PARAMETER_NUMBER_OUT_OF_RANGE =142;
public: static const int sNCE_Q_WORD_MISSING_WITH_G83 =143;
public: static const int sNCE_Q_WORD_WITH_NO_G83= 144;
public: static const int sNCE_QUEUE_IS_NOT_EMPTY_AFTER_PROBING =145;
public: static const int sNCE_R_CLEARANCE_PLANE_UNSPECIFIED_IN_CYCLE =146;
public: static const int sNCE_R_I_J_K_WORDS_ALL_MISSING_FOR_ARC =147;
public: static const int sNCE_R_LESS_THAN_X_IN_CYCLE_IN_YZ_PLANE =148;
public: static const int sNCE_R_LESS_THAN_Y_IN_CYCLE_IN_XZ_PLANE =149;
public: static const int sNCE_R_LESS_THAN_Z_IN_CYCLE_IN_XY_PLANE =150;
public: static const int sNCE_R_WORD_WITH_NO_G_CODE_THAT_USES_IT =151;
public: static const int sNCE_RADIUS_TO_END_OF_ARC_DIFFERS_FROM_RADIUS_TO_START =152;
public: static const int sNCE_RADIUS_TOO_SMALL_TO_REACH_END_POINT =153;
public: static const int sNCE_REQUIRED_PARAMETER_MISSING =154;
public: static const int sNCE_SELECTED_TOOL_SLOT_NUMBER_TOO_LARGE =155;
public: static const int sNCE_SLASH_MISSING_AFTER_FIRST_ATAN_ARGUMENT =156;
public: static const int sNCE_SPINDLE_NOT_TURNING_CLOCKWISE_IN_G84 =157;
public: static const int sNCE_SPINDLE_NOT_TURNING_IN_G86 =158;
public: static const int sNCE_SPINDLE_NOT_TURNING_IN_G87= 159;
public: static const int sNCE_SPINDLE_NOT_TURNING_IN_G88 =160;
public: static const int sNCE_SSCANF_FAILED =161;
public: static const int sNCE_START_POINT_TOO_CLOSE_TO_PROBE_POINT= 162;
public: static const int sNCE_TOO_MANY_M_CODES_ON_LINE =163;
public: static const int sNCE_TOOL_LENGTH_OFFSET_INDEX_TOO_BIG =164;
public: static const int sNCE_TOOL_MAX_TOO_LARGE =165;
public: static const int sNCE_TOOL_RADIUS_INDEX_TOO_BIG =166;
public: static const int sNCE_TOOL_RADIUS_NOT_LESS_THAN_ARC_RADIUS_WITH_COMP= 167;
public: static const int sNCE_TWO_G_CODES_USED_FROM_SAME_MODAL_GROUP= 168;
public: static const int sNCE_TWO_M_CODES_USED_FROM_SAME_MODAL_GROUP =169;
public: static const int sNCE_UNABLE_TO_OPEN_FILE= 170;
public: static const int sNCE_UNCLOSED_COMMENT_FOUND =171;
public: static const int sNCE_UNCLOSED_EXPRESSION =172;
public: static const int sNCE_UNKNOWN_G_CODE_USED =173;
public: static const int sNCE_UNKNOWN_M_CODE_USED =174;
public: static const int sNCE_UNKNOWN_OPERATION =175;
public: static const int sNCE_UNKNOWN_OPERATION_NAME_STARTING_WITH_A =176;
public: static const int sNCE_UNKNOWN_OPERATION_NAME_STARTING_WITH_M =177;
public: static const int sNCE_UNKNOWN_OPERATION_NAME_STARTING_WITH_O =178;
public: static const int sNCE_UNKNOWN_OPERATION_NAME_STARTING_WITH_X =179;
public: static const int sNCE_UNKNOWN_WORD_STARTING_WITH_A =180;
public: static const int sNCE_UNKNOWN_WORD_STARTING_WITH_C =181;
public: static const int sNCE_UNKNOWN_WORD_STARTING_WITH_E =182;
public: static const int sNCE_UNKNOWN_WORD_STARTING_WITH_F =183;
public: static const int sNCE_UNKNOWN_WORD_STARTING_WITH_L =184;
public: static const int sNCE_UNKNOWN_WORD_STARTING_WITH_R= 185;
public: static const int sNCE_UNKNOWN_WORD_STARTING_WITH_S =186;
public: static const int sNCE_UNKNOWN_WORD_STARTING_WITH_T= 187;
public: static const int sNCE_UNKNOWN_WORD_WHERE_UNARY_OPERATION_COULD_BE =188;
public: static const int sNCE_X_AND_Y_WORDS_MISSING_FOR_ARC_IN_XY_PLANE= 189;
public: static const int sNCE_X_AND_Z_WORDS_MISSING_FOR_ARC_IN_XZ_PLANE =190;
public: static const int sNCE_X_VALUE_UNSPECIFIED_IN_YZ_PLANE_CANNED_CYCLE =191;
public: static const int sNCE_X_Y_AND_Z_WORDS_ALL_MISSING_WITH_G38_2 =192;
public: static const int sNCE_Y_AND_Z_WORDS_MISSING_FOR_ARC_IN_YZ_PLANE =193;
public: static const int sNCE_Y_VALUE_UNSPECIFIED_IN_XZ_PLANE_CANNED_CYCLE =194;
public: static const int sNCE_Z_VALUE_UNSPECIFIED_IN_XY_PLANE_CANNED_CYCLE =195;
public: static const int sNCE_ZERO_OR_NEGATIVE_ARGUMENT_TO_LN =196;
public: static const int sNCE_ZERO_RADIUS_ARC =197;

public: static const int sRS274NGC_MIN_ERROR =3;
public: static const int sRS274NGC_MAX_ERROR =197;                   




};

}