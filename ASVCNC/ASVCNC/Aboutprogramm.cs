﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text;
namespace ASVCNC
{
    partial class Aboutprogramm : Form
    {
        public Aboutprogramm()
        {
            InitializeComponent();
            this.Text = String.Format("О программе {0}", AssemblyTitle);
            this.labelProductName.Text = AssemblyProduct;
            this.labelVersion.Text = String.Format("Версия {0}", AssemblyVersion);
            this.labelCopyright.Text = AssemblyCopyright;
            this.labelCompanyName.Text = AssemblyCompany;
            this.textBoxDescription.Text = AssemblyDescription;
        }

        #region Методы доступа к атрибутам сборки

        public string AssemblyTitle
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
                if (attributes.Length > 0)
                {
                    AssemblyTitleAttribute titleAttribute = (AssemblyTitleAttribute)attributes[0];
                    if (titleAttribute.Title != "")
                    {
                        return titleAttribute.Title;
                    }
                }
                return System.IO.Path.GetFileNameWithoutExtension(Assembly.GetExecutingAssembly().CodeBase);
            }
        }

        public string AssemblyVersion
        {
            get
            {
                return Assembly.GetExecutingAssembly().GetName().Version.ToString();
            }
        }

        public string AssemblyDescription
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyDescriptionAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyDescriptionAttribute)attributes[0]).Description;
            }
        }

        public string AssemblyProduct
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyProductAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyProductAttribute)attributes[0]).Product;
            }
        }

        public string AssemblyCopyright
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCopyrightAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyCopyrightAttribute)attributes[0]).Copyright;
            }
        }

        public string AssemblyCompany
        {
            get
            {
                object[] attributes = Assembly.GetExecutingAssembly().GetCustomAttributes(typeof(AssemblyCompanyAttribute), false);
                if (attributes.Length == 0)
                {
                    return "";
                }
                return ((AssemblyCompanyAttribute)attributes[0]).Company;
            }
        }
        #endregion

        private void okButton_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void Aboutprogramm_Load(object sender, EventArgs e)
        {
            textBoxDescription.BackColor = Color.White;
            textBoxDescription.ForeColor = Color.Blue;
            StringBuilder strb = new StringBuilder();
            strb.Append("ASVCNC - программа интерпретатор “Gcode” Для последующего вывода   команд на станок. Идея сделать такую программу заготовку для станка, возникла в процессе  разбора исходников системы Linuxcnc . Для os Windows похожих программ с открытыми исходниками не смог найти. В-то же время  законченные программы, как правило, сделанные под конкретное железо.");
            strb.Append(".  В этой программе использовал интерпретатор  RS274  от системы linuxcnc, немного его подправил, для компилятора  visual studio 2013. Программа ASVCNC загружает gcod интерпретирует его и вызывает canonical machining functions  с координатами или командами, которые должен выполнить станок. Функции,  оставил пустыми для вашей реализации. ");
            strb.Append("Для примера: можно сделать схему на микроконтроллере и туда передавать координаты по rs232 rs485 usb ethernet… для каждого варианта будет своя реализация. Как вариант попытаться  сделать генератор step dir используя  параллельный  порт, но это не очень хорошее решение,  Windows   для real time не подходит." + Environment.NewLine + Environment.NewLine);
            strb.Append("По использованию." + Environment.NewLine + Environment.NewLine);
            strb.Append("Если в первый раз  скачали исходники, откройте проект в visual studio 2013 express." + Environment.NewLine + Environment.NewLine);
            strb.Append("Собрать решение." + Environment.NewLine + Environment.NewLine);
            strb.Append("Если все прошло без проблем. То откройте файл  Mashine.cs. В этот файл можете добавить код, который будет взаимодействовать с вашим станком. Остальной код можно не менять. " + Environment.NewLine + Environment.NewLine);

            textBoxDescription.Text = strb.ToString();
        }
    }
}
