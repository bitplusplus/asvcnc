﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;


namespace ASVCNC
{
    public class WindowGraph : UserControl
    {
        // Модель.
        private Model model;
        // 2D представление.
        private View view;
        // Контроллер ввода пользователя.
        private Controller controller;

        // Средства рисования
        Brush background; // кисть для фона
        Pen   foreground;   // перо для линий

        // Масштабирующий коэффициент, который любой размер окна приводит к нормальному диапазону 0..1.
        // Позволяет абстрагировать изображение от размеров окна
        private float NormaScale
        {
            get
            {
                return 1f / Math.Min(this.ClientSize.Width, this.ClientSize.Height);
            }
        }

        // Инициализация окна.
        public WindowGraph()
        {
            // Заголовок
            this.Text = "3D graphics";
            // Создаём части и связываем их между собой
            this.model = new Model();
            this.view = new View(this.model);
            this.controller = new Controller(this.view);
            // Включаем двойную буферизацию рисования, чтобы изображение не "мерцало".
            this.DoubleBuffered = true;
            // Создаём средства рисования.
            this.background = new SolidBrush(Color.Black); // чёрная сплошная кисть
            this.foreground = new Pen(Color.White); // белое перо нормальной толщины
            // Обрабатываем событие изменения 2D представления.
            this.view.Updated += new EventHandler(this.View_Updated);
        }

        // Перехват нужных событий от Windows

        // Отрисовка
        protected override void OnPaint(PaintEventArgs args)
        {
            base.OnPaint(args);
            // Очищаем фон.
            args.Graphics.FillRectangle(this.background, new Rectangle(new Point(0, 0), this.ClientSize));
            // Вспомогательные коэффициенты
            float scale = this.NormaScale; // Масштаб. Избегаем двойного расчёта.
            // Сдвиги для неквадратного окна, чтобы изображение оказалось в центре.
            float shift = (this.ClientSize.Width - this.ClientSize.Height) / 2.0f;
            float shiftX = shift > 0.0f ? shift : 0.0f;
            float shiftY = shift < 0.0f ? -shift : 0.0f;
            // Рисуем все линии, которые генерирует 2D представление
            foreach (List<Pointfcl> line in this.view.Lines)
            {
                // Каждую полученную точку приводим из нормальных в координаты окна:
                // центр из середины перемещаем в левый верхний угол, разворачиваем ось Y,
                // чтобы она была направлена вниз, центрируем изображение.
                PointF previousPoint = line[0].pin;
                previousPoint.X = (previousPoint.X + 0.5f) / scale + shiftX;
                previousPoint.Y = (0.5f - previousPoint.Y) / scale + shiftY;
                for (int i = 1; i < line.Count; ++i)
                {
                    PointF nextPoint = line[i].pin;
                    nextPoint.X = (nextPoint.X + 0.5f) / scale + shiftX;
                    nextPoint.Y = (0.5f - nextPoint.Y) / scale + shiftY;
                    this.foreground.Color = line[i].clr;
                    args.Graphics.DrawLine(this.foreground, previousPoint, nextPoint);
                    previousPoint = nextPoint;
                }
            }
        }

        // Движения мыши
        protected override void OnMouseMove(MouseEventArgs args)
        {
            base.OnMouseMove(args);
            float scale = this.NormaScale; // Избегаем двойного расчёта.
            // Передаём информацию в контроллер.
            this.controller.Input(
                // Нажата ли кнопка мыши
                    args.Button == MouseButtons.Left,
                // Приведённые координаты, сдвинутые таким образом,
                // чтобы центр был в середине, а ось Y ориентирована вверх
                    new PointF((float)(args.X * scale - 0.5f), (float)(0.5f - args.Y * scale))
            );
        }

        // Изменение размеров окна
        protected override void OnResize(EventArgs args)
        {
            // При изменении размеров окна требуем от Windows перерисовки.
            this.Invalidate();
        }

        // Обработка внутренних событий

        // Изменения в 2D представлении.
        private void View_Updated(object sender, EventArgs args)
        {
            // При изменении 2D представления требуем от Windows перерисовки.
            this.Invalidate();
        }


        public void addpoint3d_(double x, double y, double z,Color prn)
        {            
            model.addpoint3d(x, y, z,prn);
        }

    }





        // Контроллер ввода пользователя
        class Controller
        {
            // Предыдущие координаты
            private PointF? previousCoordinates;
            // 2D представление, на которое влияет ввод.
            private View view;

            // Инициализация
            public Controller(View view)
            {
                this.previousCoordinates = null;
                this.view = view;
            }

            // Обработка входных данных от пользователя.
            public void Input(bool pressed, PointF coordinates)
            {
                // Пока есть нажатие, любое изменение координат рассматривается как угол полукруга,
                // на который нужно изменить углы поворота в ту или иную сторону.
                // Если нет нажатия, углы не меняются.
                if (!pressed)
                {
                    this.previousCoordinates = null;
                }
                else
                {
                    if (this.previousCoordinates.HasValue)
                    {
                        this.view.Rotate(
                                Convert.ToSingle((coordinates.X - this.previousCoordinates.Value.X) * Math.PI),
                                Convert.ToSingle((coordinates.Y - this.previousCoordinates.Value.Y) * Math.PI)
                        );
                    }
                    this.previousCoordinates = coordinates;
                }
            }
        }


        // 2D представление
        class View
        {
            // Линии для вывода
            private List<List<Pointfcl>> lines;
            // Углы ракурса обзора.
            private float horizontalAngle;
            private float verticalAngle;
            // 3D модель
            private Model model;

            // Инициализация
            public View(Model model)
            {
                this.model = model;
                this.model.Updated += new EventHandler(this.Model_Updated);
                this.lines = new List<List<Pointfcl>>();
                this.horizontalAngle = 0.0f;
                this.verticalAngle = 0.0f;
                this.Update();
            }

            // Линии для вывода
            public List<List<Pointfcl>> Lines
            {
                get
                {
                    return this.lines;
                }
            }

            // Вращение ракурса обзора.
            public void Rotate(float horizontalRotate, float verticalRotate)
            {
                // Меняем углы и устраняем многократное закручивание.
                float pi2 = Convert.ToSingle(Math.PI * 2.0);
                this.horizontalAngle += horizontalRotate;
                if (this.horizontalAngle > pi2)
                {
                    this.horizontalAngle -= pi2;
                }
                if (this.horizontalAngle < pi2)
                {
                    this.horizontalAngle += pi2;
                }
                this.verticalAngle += verticalRotate;
                if (this.verticalAngle > pi2)
                {
                    this.verticalAngle -= pi2;
                }
                if (this.verticalAngle < pi2)
                {
                    this.verticalAngle += pi2;
                }
                this.Update();
            }

            // Событие обновления
            public event EventHandler Updated;
            protected void OnUpdated()
            {
                if (this.Updated != null)
                {
                    this.Updated(this, new EventArgs());
                }
            }

            // Отображение 3D модели в 2D представление.
            private void Update()
            {
                this.lines.Clear();
                // Поворачиваем исходное 3D пространство на углы,
                // под которыми мы будем его рассматривать.
                // Отступаем по Z назад и смотрим на результат как на плоскость XY в перспективе.
                // Преобразованиям подвергается каждая точка.
                // Вспомогательные коэффициенты
                float sinV = Convert.ToSingle(Math.Sin(this.verticalAngle));
                float sinH = Convert.ToSingle(Math.Sin(this.horizontalAngle));
                float cosV = Convert.ToSingle(Math.Cos(this.verticalAngle));
                float cosH = Convert.ToSingle(Math.Cos(this.horizontalAngle));
                foreach (List<GraphLine> line3D in this.model.Lines)
                {
                    List<Pointfcl> line2D = new List<Pointfcl>();
                    foreach (GraphLine point3D in line3D)
                    {
                        // Исходная точка
                        float x0 = point3D.graphcl[0];
                        float y0 = point3D.graphcl[1];
                        float z0 = point3D.graphcl[2];
                        // Вертикальный поворот вокруг оси X исходной точки - промежуточная точка 1
                        float x1 = x0;
                        float y1 = y0 * cosV - z0 * sinV;
                        float z1 = y0 * sinV + z0 * cosV;
                        // Горизонтальный поворот вокруг оси Y промежуточной точки 1 - промежуточная точка 2
                        float x2 = x1 * cosH + z1 * sinH;
                        float y2 = y1;
                        float z2 = z1 * cosH - x1 * sinH;
                        // Отступ по Z назад и добавление перспективы
                        float p = z2 + (float)1.2; // отступаем с запасом, чтобы p не обратилось в 0.
                        // Итоговая точка
                        PointF point2D = new PointF((float)(x2 / p),(float) (y2 / p)); // по-хорошему нужно придумать обработку для p = 0
                        Pointfcl ppl = new Pointfcl();
                        ppl.pin = point2D;
                        ppl.clr = point3D.cl;
                        line2D.Add(ppl);
                    }
                    this.lines.Add(line2D);
                }
                this.OnUpdated();
            }

            // Обработка события изменение модели
            private void Model_Updated(object sender, EventArgs args)
            {
                this.Update();
                this.OnUpdated();
            }
        }

        // 3D модель
        class Model
        {
             public  const float girdxmin = -0.5f;
             public  const float girdxmax  = 0.5f; 
            // Линии координатной сетки
                private List<List<GraphLine>> grid;
            // Линия графика
            private List<GraphLine> graph;

            // Инициализация
            public Model()
            {
                // Создаём линии сетки
                this.grid = new List<List<GraphLine>>();
                for (float t = girdxmin; t <= girdxmax; t += 0.1f)
                {
                    Color clr = new Color();
                    if ((t >= -0.1f) && (t < 0.0f))
                    {
                        clr = Color.Blue; 
                    }
                    else
                    {
                        clr = Color.White;
                    }

                    GraphLine[] horizontal = new GraphLine[2];
                    horizontal[0] = new GraphLine(girdxmax, t, 0.0f, clr);
                    horizontal[1] = new GraphLine(girdxmin, t, 0.0f, clr);
                    grid.Add(horizontal.ToList());
                   

                    GraphLine[] vertical = new GraphLine[2];
                    vertical[0] = new GraphLine(t, girdxmax, 0.0f, clr);
                    vertical[1] = new GraphLine(t, girdxmin, 0.0f, clr);
                    grid.Add(vertical.ToList());                 


                }
                this.graph = new List<GraphLine>();
                GraphLine graph0 = new GraphLine((float)0, (float)0, (float)0, Color.White); 
                // Создаём начальную точку графика
                this.graph.Add(graph0);               

            }

            // Все линии в пространстве
            public List<List<GraphLine>> Lines
            {
                get
                {
                    List<List<GraphLine>> lines = new List<List<GraphLine>>(this.grid);
                    lines.Add(this.graph);
                    return lines;
                }
            }

            // Событие обновления
            public event EventHandler Updated;
            protected void OnUpdated()
            {
                if (this.Updated != null)
                {
                    this.Updated(this, new EventArgs());
                }
            }
           
            public void addpoint3d(double x, double y, double z,Color clr)
            {

                // Добавляем в график следующую точку.
                GraphLine  graph0 = new GraphLine((float)x, (float)y, (float)z,clr);
                this.graph.Add(graph0);
                this.OnUpdated();
                
            }
        }



        public class GraphLine
        {

        public Color cl;
        public float[] graphcl;
        public GraphLine()
        {

        }
        public GraphLine(float x, float y, float z, Color clo)
        {
            graphcl = new float[3];
            graphcl[0] = x;
            graphcl[1] = y;
            graphcl[2] = z;
            cl = clo;
        }

            
        }


        public class Pointfcl
        {
           public PointF pin;
            public Color clr;
           public  Pointfcl()
            {

            }

        }








    }

