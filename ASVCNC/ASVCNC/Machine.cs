﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Threading;
using System.ComponentModel;
using System.Timers;

namespace ASVCNC
{
    public delegate void delegatnextcmd();
    public delegate void delegateopdgui(StringBuilder strbr);
    public class Machine
    {
        public bool flaggcodereaddone;
        public bool flaggcodefileend;
        public bool flagmachineexecutedone;
        public bool simulation;
        public int statusexecute;
        
        public event delegatnextcmd nexcmd;
        public event delegateopdgui ubdguiposition;
        public BackgroundWorker bckwork;
        public System.Timers.Timer aTimer;

        public double fscal;
        public double sscal;
        public double feed;
        public double speed;

        public double Xposact;
        public double Yposact;
        public double Zposact;
        public double Aposact;
        public double Bposact;
        public double Cposact;

        public double Xposoffset;
        public double Yposoffset;
        public double Zposoffset;
        public double Aposoffset;
        public double Bposoffset;
        public double Cposoffset;

        public double lenght_units;
        public int plane_yxz;

        const int CANON_PLANE_XY = 1;
        const int CANON_PLANE_YZ = 2;
        const int CANON_PLANE_XZ = 3;

        const int CANON_UNITS_INCHES = 1;
        const int CANON_UNITS_MM = 2;
        const int CANON_UNITS_CM = 3;

        const int CANON_EXACT_STOP = 1;
        const int CANON_EXACT_PATH = 2;
        const int CANON_CONTINUOUS = 3;

        const int CANON_SYNCHED = 1;
        const int CANON_INDEPENDENT = 2;

        const int CANON_STOPPED = 1;
        const int CANON_CLOCKWISE = 2;
        const int CANON_COUNTERCLOCKWISE = 3;

        const int CANON_WORKPIECE = 1;
        const int CANON_XYZ = 2;

        const int CANON_SIDE_RIGHT = 1;
        const int CANON_SIDE_LEFT = 2;
        const int CANON_SIDE_OFF = 3;

        const int CANON_AXIS_X = 1;
        const int CANON_AXIS_Y = 2;
        const int CANON_AXIS_Z = 3;
        const int CANON_AXIS_A = 4;
        const int CANON_AXIS_B = 5;
        const int CANON_AXIS_C = 6;

        string[] namefunct;
        string splitdouble;
        //--------------------------------------------------------------------//
        public Machine()
        {
            try
            {
                double dab = Convert.ToDouble("1.01");
                splitdouble = ".";
            }
            catch (Exception e)
            {
                splitdouble = ",";
            }
            //----------------------------------------------------------------//       

            namefunct = new string[40];
            namefunct[0] = "SET_ORIGIN_OFFSETS";
            namefunct[1] = "USE_LENGTH_UNITS";
            namefunct[2] = "STRAIGHT_TRAVERSE";
            namefunct[3] = "SELECT_PLANE";
            namefunct[4] = "SET_FEED_RATE";
            namefunct[5] = "SET_FEED_REFERENCE";
            namefunct[6] = "SET_MOTION_CONTROL_MODE";
            namefunct[7] = "START_SPEED_FEED_SYNCH";
            namefunct[8] = "STOP_SPEED_FEED_SYNCH";
            namefunct[9] = "ARC_FEED";
            namefunct[10] = "DWELL";
            namefunct[11] = "STRAIGHT_FEED";
            namefunct[12] = "STRAIGHT_PROBE";
            namefunct[13] = "ORIENT_SPINDLE";
            namefunct[14] = "SET_SPINDLE_SPEED";
            namefunct[15] = "START_SPINDLE_CLOCKWISE";
            namefunct[16] = "START_SPINDLE_COUNTERCLOCKWISE";
            namefunct[17] = "STOP_SPINDLE_TURNING";
            namefunct[18] = "CHANGE_TOOL";
            namefunct[19] = "SELECT_TOOL";
            namefunct[20] = "USE_TOOL_LENGTH_OFFSET";
            namefunct[21] = "COMMENT";
            namefunct[22] = "DISABLE_FEED_OVERRIDE";
            namefunct[23] = "DISABLE_SPEED_OVERRIDE";
            namefunct[24] = "ENABLE_FEED_OVERRIDE";
            namefunct[25] = "ENABLE_SPEED_OVERRIDE";
            namefunct[26] = "FLOOD_OFF";
            namefunct[27] = "FLOOD_ON";
            namefunct[28] = "INIT_CANON";
            namefunct[29] = "MESSAGE";
            namefunct[30] = "MIST_OFF";
            namefunct[31] = "MIST_ON";
            namefunct[32] = "PALLET_SHUTTLE";
            namefunct[33] = "OPTIONAL_PROGRAM_STOP";
            namefunct[34] = "PROGRAM_END";
            namefunct[35] = "PROGRAM_STO";
            namefunct[36] = "";
            namefunct[37] = "";
            namefunct[38] = "";
            namefunct[39] = "";


            flaggcodereaddone = false;
            flaggcodefileend = false;
            simulation = false;
            flagmachineexecutedone = false;
            statusexecute = 0;
            bckwork = new BackgroundWorker();

            bckwork.DoWork += new DoWorkEventHandler(bckwork_dowork);
            bckwork.ProgressChanged += new ProgressChangedEventHandler(bckwork_ProgressChanged);
            bckwork.RunWorkerCompleted += new RunWorkerCompletedEventHandler(bckwork_RunWorkerCompleted);

            bckwork.WorkerReportsProgress = true;
            bckwork.WorkerSupportsCancellation = true;

            aTimer = new System.Timers.Timer(10000);
            aTimer.Elapsed += new  ElapsedEventHandler(OnTimedEvent);
            aTimer.Interval = 10;
            aTimer.Enabled = true;

            fscal = 1.0;
            sscal = 1.0;
            feed  = 0.0;
            speed = 0.0;

            Xposact = 0.0;
            Yposact = 0.0;
            Zposact = 0.0;
            Aposact = 0.0;
            Bposact = 0.0;
            Cposact = 0.0;

            Xposoffset = 0.0;
            Yposoffset = 0.0;
            Zposoffset = 0.0;
            Aposoffset = 0.0;
            Bposoffset = 0.0;
            Cposoffset = 0.0;

            lenght_units = 0;
        }//construct
        //--------------------------------------------------------------------//
        int countfunction = 0;
        public void interpretertexttofunction(string strr)
        {
            
            int indexnamefunct = -1;
            for(int ii = 0; ii < 36; ii ++ )
            {               
                if(Regex.IsMatch(strr,  namefunct[ii]))
                {
                    indexnamefunct = ii;
                    countfunction++;
                    break;
                }else
                {
                    indexnamefunct = -1;
                }

            }//for

            switch(indexnamefunct)
            {

                case 0:
                    {

                        Match match = Regex.Match(strr, @"^([\d\s]{6}N..... SET_ORIGIN_OFFSETS\()\s*([\-\.\d]*)\s*,\s*([\-\.\d]*)\s*,\s*([\-\.\d]*)\s*,\s*([\-\.\d]*)\s*,\s*([\-\.\d]*)\s*,\s*([\-\.\d]*)\s*");

                        double xx = Convert.ToDouble(match.Groups[2].Value.Replace(".", splitdouble).Replace(",", splitdouble));
                        double yy = Convert.ToDouble(match.Groups[3].Value.Replace(".", splitdouble).Replace(",", splitdouble));
                        double zz = Convert.ToDouble(match.Groups[4].Value.Replace(".", splitdouble).Replace(",", splitdouble));
                        double aa = Convert.ToDouble(match.Groups[5].Value.Replace(".", splitdouble).Replace(",", splitdouble));
                        double bb = Convert.ToDouble(match.Groups[6].Value.Replace(".", splitdouble).Replace(",", splitdouble));
                        double cc = Convert.ToDouble(match.Groups[7].Value.Replace(".", splitdouble).Replace(",", splitdouble));

                        SET_ORIGIN_OFFSETS(xx,yy,zz,aa,bb,cc);
                        break;
                      
                    }//case 

                case 1:
                    {
                        Match match = Regex.Match(strr, @"^([\d\s]{6}N..... USE_LENGTH_UNITS\()\s*([\w]*)\s*");
                        string units = match.Groups[2].Value;

                        if(units.Equals("CANON_UNITS_INCHES"))
                        {
                            USE_LENGTH_UNITS(CANON_UNITS_INCHES);
                        }
                        else if (units.Equals("CANON_UNITS_MM"))
                        {
                            USE_LENGTH_UNITS(CANON_UNITS_MM);

                        }else if (units.Equals("CANON_UNITS_CM"))
                        {
                            USE_LENGTH_UNITS(CANON_UNITS_CM);
                        }

                        break;
                    }//case
                case 2:
                    {

                        Match match = Regex.Match(strr, @"^([\d\s]{6}N..... STRAIGHT_TRAVERSE\()\s*([\-\.\d]*)\s*,\s*([\-\.\d]*)\s*,\s*([\-\.\d]*)\s*,\s*([\-\.\d]*)\s*,\s*([\-\.\d]*)\s*,\s*([\-\.\d]*)\s*");

                        double xx = Convert.ToDouble(match.Groups[2].Value.Replace(".", splitdouble).Replace(",", splitdouble));
                        double yy = Convert.ToDouble(match.Groups[3].Value.Replace(".", splitdouble).Replace(",", splitdouble));
                        double zz = Convert.ToDouble(match.Groups[4].Value.Replace(".", splitdouble).Replace(",", splitdouble));
                        double aa = Convert.ToDouble(match.Groups[5].Value.Replace(".", splitdouble).Replace(",", splitdouble));
                        double bb = Convert.ToDouble(match.Groups[6].Value.Replace(".", splitdouble).Replace(",", splitdouble));
                        double cc = Convert.ToDouble(match.Groups[7].Value.Replace(".", splitdouble).Replace(",", splitdouble));

                        STRAIGHT_TRAVERSE(xx,yy,zz,aa,bb,cc);
                        break;
                    }//case
                case 3:
                    {

                        Match match = Regex.Match(strr, @"^([\d\s]{6}N..... SELECT_PLANE\()\s*([\w]*)\s*");
                        string  plane = match.Groups[2].Value;

                        if (plane.Equals("CANON_PLANE_XY"))
                        {
                            SELECT_PLANE(CANON_PLANE_XY);
                        }
                        else if (plane.Equals("CANON_PLANE_YZ"))
                        {
                            SELECT_PLANE(CANON_PLANE_YZ);
                        }
                        else if (plane.Equals("CANON_PLANE_XZ"))
                        {
                            SELECT_PLANE(CANON_PLANE_XZ);
                        }
                       
                        break;
                    }//case
                case 4:
                    {

                        Match match = Regex.Match(strr, @"^([\d\s]{6}N..... SET_FEED_RATE\()\s*([\-\.\d]*)\s*");
                        double  rate = Convert.ToDouble(match.Groups[2].Value.Replace(".", splitdouble).Replace(",", splitdouble));

                        SET_FEED_RATE(rate);
                        break;
                    }//case
                case 5:
                    {

                         Match match = Regex.Match(strr, @"^([\d\s]{6}N..... SET_FEED_REFERENCE\()\s*([\w]*)\s*");
                         string  reference = match.Groups[2].Value;

                         if (reference.Equals("CANON_WORKPIECE"))
                         {
                             SET_FEED_REFERENCE(CANON_WORKPIECE);
                         }
                         else if (reference.Equals("CANON_XYZ"))
                         {
                             SET_FEED_REFERENCE(CANON_XYZ);
                         }
                         
                        break;
                    }//case
                case 6:
                    {

                        Match match = Regex.Match(strr, @"^([\d\s]{6}N..... SET_MOTION_CONTROL_MODE\()\s*([\w]*)\s*");
                        string  mode = match.Groups[2].Value;

                        if(mode.Equals("CANON_EXACT_STOP"))
                        {
                            SET_MOTION_CONTROL_MODE(CANON_EXACT_STOP);
                        }
                        else if (mode.Equals("CANON_EXACT_PATH"))
                        {
                            SET_MOTION_CONTROL_MODE(CANON_EXACT_PATH);
                        }
                        else if (mode.Equals("CANON_CONTINUOUS"))
                        {
                            SET_MOTION_CONTROL_MODE(CANON_CONTINUOUS);
                        }

                        break;
                    }//case
                case 7:
                    {

                        START_SPEED_FEED_SYNCH();
                        break;
                    }//case

                case 8:
                    {
                         STOP_SPEED_FEED_SYNCH();
                        break;
                    }//case

                case 9:
                    {

                        Match match = Regex.Match(strr, @"^([\d\s]{6}N..... ARC_FEED\()\s*([\-\.\d]*)\s*,\s*([\-\.\d]*)\s*,\s*([\-\.\d]*)\s*,\s*([\-\.\d]*)\s*,\s*([\-\d]*)\s*,\s*([\-\.\d]*)\s*,\s*([\-\.\d]*)\s*,\s*([\-\.\d]*)\s*,\s*([\-\.\d]*)\s*");

                        double first_end = Convert.ToDouble(match.Groups[2].Value.Replace(".", splitdouble).Replace(",", splitdouble));
                        double second_end = Convert.ToDouble(match.Groups[3].Value.Replace(".", splitdouble).Replace(",", splitdouble));
                        double first_axis = Convert.ToDouble(match.Groups[4].Value.Replace(".", splitdouble).Replace(",", splitdouble));
                        double second_axis = Convert.ToDouble(match.Groups[5].Value.Replace(".", splitdouble).Replace(",", splitdouble));
                        int rotation = Convert.ToInt32(match.Groups[6].Value);
                        double axis_end_point = Convert.ToDouble(match.Groups[7].Value.Replace(".", splitdouble).Replace(",", splitdouble));
                        double aa = Convert.ToDouble(match.Groups[8].Value.Replace(".", splitdouble).Replace(",", splitdouble));
                        double bb = Convert.ToDouble(match.Groups[9].Value.Replace(".", splitdouble).Replace(",", splitdouble));
                        double cc = Convert.ToDouble(match.Groups[10].Value.Replace(".", splitdouble).Replace(",", splitdouble));

                        ARC_FEED(first_end,second_end,first_axis,second_axis,rotation,axis_end_point,aa, bb,cc);    
                   break;
                    }//case
                case 10:
                    {

                        Match match = Regex.Match(strr, @"^([\d\s]{6}N..... DWELL\()\s*([\-\.\d]*)\s*");
                        double  seconds = Convert.ToDouble(match.Groups[2].Value.Replace(".", splitdouble).Replace(",", splitdouble));

                        DWELL(seconds);
                        break;
                    }//case
                case 11:
                    {

                        Match match = Regex.Match(strr, @"^([\d\s]{6}N..... STRAIGHT_FEED\()\s*([\-\.\d]*)\s*,\s*([\-\.\d]*)\s*,\s*([\-\.\d]*)\s*,\s*([\-\.\d]*)\s*,\s*([\-\.\d]*)\s*,\s*([\-\.\d]*)\s*");

                        double xx = Convert.ToDouble(match.Groups[2].Value.Replace(".", splitdouble).Replace(",", splitdouble));
                        double yy = Convert.ToDouble(match.Groups[3].Value.Replace(".", splitdouble).Replace(",", splitdouble));
                        double zz = Convert.ToDouble(match.Groups[4].Value.Replace(".", splitdouble).Replace(",", splitdouble));
                        double aa = Convert.ToDouble(match.Groups[5].Value.Replace(".", splitdouble).Replace(",", splitdouble));
                        double bb = Convert.ToDouble(match.Groups[6].Value.Replace(".", splitdouble).Replace(",", splitdouble));
                        double cc = Convert.ToDouble(match.Groups[7].Value.Replace(".", splitdouble).Replace(",", splitdouble));

                        STRAIGHT_FEED(xx,yy,zz,aa,bb,cc);
                        break;
                    }//case
                case 12:
                    {

                        Match match = Regex.Match(strr, @"^([\d\s]{6}N..... STRAIGHT_PROBE\()\s*([\-\.\d]*)\s*,\s*([\-\.\d]*)\s*,\s*([\-\.\d]*)\s*,\s*([\-\.\d]*)\s*,\s*([\-\.\d]*)\s*,\s*([\-\.\d]*)\s*");

                        double xx = Convert.ToDouble(match.Groups[2].Value.Replace(".", splitdouble).Replace(",", splitdouble));
                        double yy = Convert.ToDouble(match.Groups[3].Value.Replace(".", splitdouble).Replace(",", splitdouble));
                        double zz = Convert.ToDouble(match.Groups[4].Value.Replace(".", splitdouble).Replace(",", splitdouble));
                        double aa = Convert.ToDouble(match.Groups[5].Value.Replace(".", splitdouble).Replace(",", splitdouble));
                        double bb = Convert.ToDouble(match.Groups[6].Value.Replace(".", splitdouble).Replace(",", splitdouble));
                        double cc = Convert.ToDouble(match.Groups[7].Value.Replace(".", splitdouble).Replace(",", splitdouble));

                        STRAIGHT_PROBE(xx, yy, zz, aa, bb, cc);
                        break;
                    }//case
                case 13:
                    {

                        Match match = Regex.Match(strr, @"^([\d\s]{6}N..... ORIENT_SPINDLE\()\s*([\-\.\d]*)\s*,\s*([\w]*)\s*");

                        double orientation = Convert.ToDouble(match.Groups[2].Value.Replace(".", splitdouble).Replace(",", splitdouble));
                        string direction = match.Groups[3].Value;

                        if(direction.Equals("CANON_STOPPED"))
                        {
                            ORIENT_SPINDLE(orientation, CANON_STOPPED);
                        }
                        else if(direction.Equals("CANON_CLOCKWISE"))
                        {
                            ORIENT_SPINDLE(orientation, CANON_CLOCKWISE);
                        }
                        else if(direction.Equals("CANON_COUNTERCLOCKWISE"))
                        {
                            ORIENT_SPINDLE(orientation, CANON_COUNTERCLOCKWISE);
                        }

                        
                        break;
                    }//case
                case 14:
                    {

                        Match match = Regex.Match(strr, @"^([\d\s]{6}N..... SET_SPINDLE_SPEED\()\s*([\-\.\d]*)\s*");
                        double  r = Convert.ToDouble(match.Groups[2].Value.Replace(".", splitdouble).Replace(",", splitdouble));
                        
                        SET_SPINDLE_SPEED(r);
                        break;
                    }//case
                case 15:
                    {
                        START_SPINDLE_CLOCKWISE();
                        break;
                    }//case
                case 16:
                    {
                        START_SPINDLE_COUNTERCLOCKWISE() ;
                        break;
                    }//case

                case 17:
                    {

                        STOP_SPINDLE_TURNING();
                        break;
                    }//case
                case 18:
                    {
                        Match match = Regex.Match(strr, @"^([\d\s]{6}N..... CHANGE_TOOL\()\s*([\-\d]*)\s*");
                        int  slot = Convert.ToInt32(match.Groups[2].Value);
                        
                        CHANGE_TOOL(slot);
                        break;
                    }//case
                case 19:
                    {

                        Match match = Regex.Match(strr, @"^([\d\s]{6}N..... SELECT_TOOL\()\s*([\-\d]*)\s*");
                        int  ii = Convert.ToInt32(match.Groups[2].Value);

                        SELECT_TOOL(ii);
                        break;
                    }//case
                case 20:
                    {

                        Match match = Regex.Match(strr, @"^([\d\s]{6}N..... USE_TOOL_LENGTH_OFFSET\()\s*([\-\.\d]*)\s*");
                        double  offset = Convert.ToDouble(match.Groups[2].Value.Replace(".", splitdouble).Replace(",", splitdouble));

                        USE_TOOL_LENGTH_OFFSET(offset);
                        break;
                    }//case
                case 21:
                    {

                        Match match = Regex.Match(strr, @"^([\d\s]{6}N..... COMMENT\()\s*([\w]*)\s*");
                        string  ss = match.Groups[2].Value;

                        COMMENT(ss);
                        break;
                    }//case
                case 22:
                    {

                        DISABLE_FEED_OVERRIDE();
                        break;
                    }//case
                case 23:
                    {

                        DISABLE_SPEED_OVERRIDE();
                        break;
                    }//case
                case 24:
                    {
                        ENABLE_FEED_OVERRIDE();
                        break;
                    }//case

                case 25:
                    {

                        ENABLE_SPEED_OVERRIDE();
                        break;
                    }//case
                case 26:
                    {
                        FLOOD_OFF();
                        break;
                    }//case
                case 27:
                    {

                        FLOOD_ON();
                        break;
                    }//case
                case 28:
                    {

                        INIT_CANON();
                        break;
                    }//case
                case 29:
                    {

                        Match match = Regex.Match(strr, @"^([\d\s]{6}N..... MESSAGE\()\s*([\w]*)\s*");
                        string  ss = match.Groups[2].Value;

                        MESSAGE(ss);
                        break;
                    }//case
                case 30:
                    {
                        MIST_OFF();
                        break;
                    }//case
                case 31:
                    {

                        MIST_ON();
                        break;
                    }//case
                case 32:
                    {

                        PALLET_SHUTTLE();
                        break;
                    }//case

                case 33:
                    {

                        OPTIONAL_PROGRAM_STOP();
                        break;
                    }//case
                case 34:
                    {

                        PROGRAM_END();
                        break;
                    }//case
                case 35:
                    {

                        PROGRAM_STOP();
                        break;
                    }//case

                default: break;
            }//end switch

        }//interpreter
        //--------------------------------------------------------------------//
        private void bckwork_dowork(object sender, DoWorkEventArgs e)
        {

            for (; ; )
            {

                switch (statusexecute)
                {
                    case 0:
                        {

                            flaggcodefileend = false;
                            flaggcodereaddone = false;
                            flagmachineexecutedone  = false;
                            countfunction = 0;
                            break;
                        }

                    case 1:
                        {
                            if (flaggcodereaddone)
                            {
                                statusexecute = 2;
                            }
                            break;
                        }

                    case 2:
                        {
                            if (flaggcodefileend)
                            {
                                statusexecute = 4;
                            }
                            else
                            {
                                statusexecute = 3;
                            }
                            break;
                        }

                    case 3:
                        {
                            if ((flagmachineexecutedone) && (countfunction<=0))
                            {
                                flagmachineexecutedone = false;
                                statusexecute = 5;                               
                            }
                            break;
                        }
                   case 4:
                        {
                            if ((flagmachineexecutedone) && (countfunction <= 0))
                            {
                                flagmachineexecutedone = false;
                                statusexecute = 0;                               
                            }
                            break;
                        }
                    case 5:
                        {
                            statusexecute = 1;
                            nexcmd();
                            break;
                        }

                    default: break;
                }//switch





            }//for

        }
        //--------------------------------------------------------------------//
        void bckwork_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {

        }
        //--------------------------------------------------------------------//
        void bckwork_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {

        }
        //--------------------------------------------------------------------//
        private void OnTimedEvent(object source, ElapsedEventArgs e)
        {


                if ((countfunction <= 0) &&(statusexecute>1))
                {
                    flagmachineexecutedone = true;
                    countfunction = 0;
                }

             positiongui();
        }
        //--------------------------------------------------------------------//
        private void positiongui()
        {
            StringBuilder str = new StringBuilder();
            str.Append("X = ("+  String.Format("{0:0000.000}",(Xposact + Xposoffset)) + ") ");
            str.Append("Y = (" + String.Format("{0:0000.000}",(Yposact + Yposoffset)) + ") ");
            str.Append("Z = (" + String.Format("{0:0000.000}",(Zposact + Zposoffset)) + ") ");
            str.Append("A = (" + String.Format("{0:0000.000}",(Aposact + Aposoffset)) + ") ");
            str.Append("B = (" + String.Format("{0:0000.000}",(Bposact + Bposoffset)) + ") ");
            str.Append("C = (" + String.Format("{0:0000.000}",(Cposact + Cposoffset)) + ") ");

            ubdguiposition(str);
        }
         //--------------------------------------------------------------------//       
        private void machin_nex(bool rdy)
        {
            if (simulation)
            {
                countfunction--;
            }
            else
            {
                if (rdy)
                {
                    countfunction--;
                }
            }
        }
        //-----------------------------------------------------------------------------------------------//
        //-----------------------------------------------------------------------------------------------//
        //-----------------------------------------------------------------------------------------------//
        //-----------------------------------------------------------------------------------------------//

        //код который ниже будет взаимодействовать с вашим станком, остальной код можно не менять .  

        //-----------------------------------------------------------------------------------------------//
        //-----------------------------------------------------------------------------------------------//
        //-----------------------------------------------------------------------------------------------//
        //-----------------------------------------------------------------------------------------------//
        private void SET_ORIGIN_OFFSETS(double x, double y, double z, double a, double b, double c)
        {

            Xposoffset = x;
            Yposoffset = y;
            Zposoffset = z;
            Aposoffset = a;
            Bposoffset = b;
            Cposoffset = c;

            machin_nex(true);

        }//!
        //--------------------------------------------------------------------//
        private void USE_LENGTH_UNITS(int units)
        {
            
            switch(units)
            {
                case CANON_UNITS_INCHES:
                    {
                        lenght_units = 2.54;
                        break;
                    }

                case CANON_UNITS_MM:
                    {

                        lenght_units = 1;

                        break;
                    }

                    case CANON_UNITS_CM:
                    {
                        lenght_units = 10;
                        break;
                    }

                default: break;
            }

            machin_nex(true);
        }//!
        //--------------------------------------------------------------------//
        private void STRAIGHT_TRAVERSE(double x, double y, double z, double a, double b, double c)
        {

            machin_nex(false);
        }//!
        //--------------------------------------------------------------------//
        private void SELECT_PLANE(int plane)
        {
            plane_yxz = plane;

            switch(plane)
            {
                case CANON_PLANE_XY:
                    {

                        break;
                    }
                case CANON_PLANE_YZ:
                    {

                        break;
                    }
                case CANON_PLANE_XZ:
                    {

                        break;
                    }

                default: break;
            }

            machin_nex(true);
        }//!
        //--------------------------------------------------------------------//
        private void SET_FEED_RATE(double rate)
        {
            feed = rate * fscal;
            machin_nex(true);
        }//!
        //--------------------------------------------------------------------//
        private void SET_FEED_REFERENCE(int reference)
        {

            machin_nex(true);
        }//!
        //--------------------------------------------------------------------//
        private void SET_MOTION_CONTROL_MODE(int mode)
        {

            switch(mode)
            {
                case CANON_EXACT_STOP:
                    {

                        break;
                    }
                case CANON_EXACT_PATH:
                    {

                        break;
                    }
                case CANON_CONTINUOUS:
                    {

                        break;
                    }

                default: break;
            }//switch


            machin_nex(true);
        }//!
        //--------------------------------------------------------------------//
        private void START_SPEED_FEED_SYNCH()
        {

            machin_nex(true);
        }//!
        //--------------------------------------------------------------------//
        private void STOP_SPEED_FEED_SYNCH()
        {

            machin_nex(true);
        }//!
        //--------------------------------------------------------------------//
        private void ARC_FEED(double first_end, double second_end, double first_axis, double second_axis, int rotation, double axis_end_point, double a, double b, double c)
        {

            machin_nex(false);
        }//!
        //--------------------------------------------------------------------//
        private void DWELL(double seconds)
        {

            machin_nex(true);
        }//!
        //--------------------------------------------------------------------//
        private void STRAIGHT_FEED(double x, double y, double z, double a, double b, double c)
        {

            machin_nex(false);
        }//!
        //--------------------------------------------------------------------//
        private void STRAIGHT_PROBE(double x, double y, double z, double a, double b, double c)
        {

            machin_nex(false);
        }//!
        //--------------------------------------------------------------------//
        private void ORIENT_SPINDLE(double orientation, int direction)
        {
            machin_nex(false);
        }//!
        //--------------------------------------------------------------------//
        private void SET_SPINDLE_SPEED(double r)
        {
            speed = r * sscal;
            machin_nex(true);
        }//!
        //--------------------------------------------------------------------//
        private void START_SPINDLE_CLOCKWISE()
        {
            machin_nex(true);
        }//!
        //--------------------------------------------------------------------//
        private void START_SPINDLE_COUNTERCLOCKWISE()
        {
            machin_nex(true);
        }//!
        //--------------------------------------------------------------------//
        private void STOP_SPINDLE_TURNING()
        {

            machin_nex(true);
        }//!
        //--------------------------------------------------------------------//
        private void CHANGE_TOOL(int slot)
        {
            machin_nex(true);
        }//!
        //--------------------------------------------------------------------//
        private void SELECT_TOOL(int i)
        {

            machin_nex(true);
        }//!
        //--------------------------------------------------------------------//
        private void USE_TOOL_LENGTH_OFFSET(double offset)
        {

            machin_nex(true);
        }//!
        //--------------------------------------------------------------------//
        private void COMMENT(String s)
        {
            machin_nex(true);
        }//!
        //--------------------------------------------------------------------//
        private void DISABLE_FEED_OVERRIDE()
        {

            machin_nex(true);
        }//!
        //--------------------------------------------------------------------//
        private void DISABLE_SPEED_OVERRIDE()
        {

            machin_nex(true);
        }//!
        //--------------------------------------------------------------------//
        private void ENABLE_FEED_OVERRIDE()
        {

            machin_nex(true);
        }//!
        //--------------------------------------------------------------------//
        private void ENABLE_SPEED_OVERRIDE()
        {

            machin_nex(true);
        }//!
        //--------------------------------------------------------------------//
        private void FLOOD_OFF()
        {
            machin_nex(true);
        }//!
        //--------------------------------------------------------------------//
        private void FLOOD_ON()
        {

            machin_nex(true);
        }//!
        //--------------------------------------------------------------------//
        private void INIT_CANON()
        {

            machin_nex(true);
        }//!
        //--------------------------------------------------------------------//
        private void MESSAGE(String s)
        {
            machin_nex(true);
        }//!
        //--------------------------------------------------------------------//
        private void MIST_OFF()
        {

            machin_nex(true);
        }//!
        //--------------------------------------------------------------------//
        private void MIST_ON()
        {

            machin_nex(true);
        }//!
        //--------------------------------------------------------------------//
        private void PALLET_SHUTTLE()
        {

            machin_nex(true);
        }//!
        //--------------------------------------------------------------------//
        private void OPTIONAL_PROGRAM_STOP()
        {

            machin_nex(true);
        }//!
        //--------------------------------------------------------------------//
        private void PROGRAM_END()
        {
            machin_nex(true);
        }//!
        //--------------------------------------------------------------------//
        private void PROGRAM_STOP()
        {
            machin_nex(true);
        }//!!
        //--------------------------------------------------------------------//
    }//class
}//end


