﻿namespace ASVCNC
{
    partial class ASVCNC
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.statusStripasvcnc = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelcountstrasvcnc = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelcountgcodstr = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelselectgcode = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelnumselectstr = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelgmsfcodes = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabelgmsfcod = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripasvcnc = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonopengcod = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtoneditgcode = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonclosegcode = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtontool = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonrun = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonsteprun = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonstop = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonsimulat = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonparamm = new System.Windows.Forms.ToolStripButton();
            this.toolStripButtonrelog = new System.Windows.Forms.ToolStripButton();
            this.panelasvcnc = new System.Windows.Forms.Panel();
            this.tabControlkinematic = new System.Windows.Forms.TabControl();
            this.tabPagexyz = new System.Windows.Forms.TabPage();
            this.tabPageabc = new System.Windows.Forms.TabPage();
            this.toolStripfactpol = new System.Windows.Forms.ToolStrip();
            this.toolStripLabelfactpol = new System.Windows.Forms.ToolStripLabel();
            this.toolStrippositionprog = new System.Windows.Forms.ToolStrip();
            this.toolStripLabelpositionprog = new System.Windows.Forms.ToolStripLabel();
            this.tabControlasvcncmanualauto = new System.Windows.Forms.TabControl();
            this.tabPageauto = new System.Windows.Forms.TabPage();
            this.richTextBoxGcode = new System.Windows.Forms.RichTextBox();
            this.contextMenuStripgcode = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemrunthis = new System.Windows.Forms.ToolStripMenuItem();
            this.tabPagemanual = new System.Windows.Forms.TabPage();
            this.labelregs = new System.Windows.Forms.Label();
            this.trackBarregs = new System.Windows.Forms.TrackBar();
            this.labelregf = new System.Windows.Forms.Label();
            this.trackBarfscal = new System.Windows.Forms.TrackBar();
            this.buttonrun = new System.Windows.Forms.Button();
            this.labelcmd = new System.Windows.Forms.Label();
            this.textBoxcmd = new System.Windows.Forms.TextBox();
            this.richTextBoxlogasvcnc = new System.Windows.Forms.RichTextBox();
            this.contextMenuStripasvcnclog = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toolStripMenuItemlogclear = new System.Windows.Forms.ToolStripMenuItem();
            this.checkBoxlog = new System.Windows.Forms.CheckBox();
            this.toolStripButtonabout = new System.Windows.Forms.ToolStripButton();
            this.statusStripasvcnc.SuspendLayout();
            this.toolStripasvcnc.SuspendLayout();
            this.panelasvcnc.SuspendLayout();
            this.tabControlkinematic.SuspendLayout();
            this.toolStripfactpol.SuspendLayout();
            this.toolStrippositionprog.SuspendLayout();
            this.tabControlasvcncmanualauto.SuspendLayout();
            this.tabPageauto.SuspendLayout();
            this.contextMenuStripgcode.SuspendLayout();
            this.tabPagemanual.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarregs)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarfscal)).BeginInit();
            this.contextMenuStripasvcnclog.SuspendLayout();
            this.SuspendLayout();
            // 
            // statusStripasvcnc
            // 
            this.statusStripasvcnc.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelcountstrasvcnc,
            this.toolStripStatusLabelcountgcodstr,
            this.toolStripStatusLabelselectgcode,
            this.toolStripStatusLabelnumselectstr,
            this.toolStripStatusLabelgmsfcodes,
            this.toolStripStatusLabelgmsfcod});
            this.statusStripasvcnc.Location = new System.Drawing.Point(0, 707);
            this.statusStripasvcnc.Name = "statusStripasvcnc";
            this.statusStripasvcnc.Size = new System.Drawing.Size(1008, 22);
            this.statusStripasvcnc.TabIndex = 0;
            this.statusStripasvcnc.Text = "Строка статуса";
            // 
            // toolStripStatusLabelcountstrasvcnc
            // 
            this.toolStripStatusLabelcountstrasvcnc.Name = "toolStripStatusLabelcountstrasvcnc";
            this.toolStripStatusLabelcountstrasvcnc.Size = new System.Drawing.Size(145, 17);
            this.toolStripStatusLabelcountstrasvcnc.Text = "Количество строк gcode:";
            // 
            // toolStripStatusLabelcountgcodstr
            // 
            this.toolStripStatusLabelcountgcodstr.Name = "toolStripStatusLabelcountgcodstr";
            this.toolStripStatusLabelcountgcodstr.Size = new System.Drawing.Size(43, 17);
            this.toolStripStatusLabelcountgcodstr.Text = "000000";
            // 
            // toolStripStatusLabelselectgcode
            // 
            this.toolStripStatusLabelselectgcode.Name = "toolStripStatusLabelselectgcode";
            this.toolStripStatusLabelselectgcode.Size = new System.Drawing.Size(52, 17);
            this.toolStripStatusLabelselectgcode.Text = "Строка :";
            // 
            // toolStripStatusLabelnumselectstr
            // 
            this.toolStripStatusLabelnumselectstr.Name = "toolStripStatusLabelnumselectstr";
            this.toolStripStatusLabelnumselectstr.Size = new System.Drawing.Size(43, 17);
            this.toolStripStatusLabelnumselectstr.Text = "000000";
            // 
            // toolStripStatusLabelgmsfcodes
            // 
            this.toolStripStatusLabelgmsfcodes.Name = "toolStripStatusLabelgmsfcodes";
            this.toolStripStatusLabelgmsfcodes.Size = new System.Drawing.Size(127, 17);
            this.toolStripStatusLabelgmsfcodes.Text = "Коды G / M / F / S / T :";
            // 
            // toolStripStatusLabelgmsfcod
            // 
            this.toolStripStatusLabelgmsfcod.Name = "toolStripStatusLabelgmsfcod";
            this.toolStripStatusLabelgmsfcod.Size = new System.Drawing.Size(21, 17);
            this.toolStripStatusLabelgmsfcod.Text = "G0";
            // 
            // toolStripasvcnc
            // 
            this.toolStripasvcnc.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonopengcod,
            this.toolStripButtoneditgcode,
            this.toolStripButtonclosegcode,
            this.toolStripButtontool,
            this.toolStripButtonrun,
            this.toolStripButtonsteprun,
            this.toolStripButtonstop,
            this.toolStripButtonsimulat,
            this.toolStripButtonparamm,
            this.toolStripButtonrelog,
            this.toolStripButtonabout});
            this.toolStripasvcnc.Location = new System.Drawing.Point(0, 0);
            this.toolStripasvcnc.Name = "toolStripasvcnc";
            this.toolStripasvcnc.Size = new System.Drawing.Size(1008, 55);
            this.toolStripasvcnc.TabIndex = 1;
            this.toolStripasvcnc.Text = "Панель управления";
            // 
            // toolStripButtonopengcod
            // 
            this.toolStripButtonopengcod.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonopengcod.Image = global::ASVCNC.Properties.Resources.open;
            this.toolStripButtonopengcod.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonopengcod.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonopengcod.Margin = new System.Windows.Forms.Padding(10, 1, 0, 2);
            this.toolStripButtonopengcod.Name = "toolStripButtonopengcod";
            this.toolStripButtonopengcod.Size = new System.Drawing.Size(52, 52);
            this.toolStripButtonopengcod.Text = "Открыть Gcod";
            this.toolStripButtonopengcod.Click += new System.EventHandler(this.toolStripButtonopengcod_Click);
            // 
            // toolStripButtoneditgcode
            // 
            this.toolStripButtoneditgcode.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtoneditgcode.Image = global::ASVCNC.Properties.Resources.edit;
            this.toolStripButtoneditgcode.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtoneditgcode.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtoneditgcode.Margin = new System.Windows.Forms.Padding(10, 1, 0, 2);
            this.toolStripButtoneditgcode.Name = "toolStripButtoneditgcode";
            this.toolStripButtoneditgcode.Size = new System.Drawing.Size(52, 52);
            this.toolStripButtoneditgcode.Text = "Редактировать gcod";
            this.toolStripButtoneditgcode.Click += new System.EventHandler(this.toolStripButtoneditgcode_Click);
            // 
            // toolStripButtonclosegcode
            // 
            this.toolStripButtonclosegcode.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonclosegcode.Image = global::ASVCNC.Properties.Resources.folder_delete;
            this.toolStripButtonclosegcode.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonclosegcode.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonclosegcode.Margin = new System.Windows.Forms.Padding(10, 1, 0, 2);
            this.toolStripButtonclosegcode.Name = "toolStripButtonclosegcode";
            this.toolStripButtonclosegcode.Size = new System.Drawing.Size(52, 52);
            this.toolStripButtonclosegcode.Text = "Закрыть Gcod";
            this.toolStripButtonclosegcode.Click += new System.EventHandler(this.toolStripButtonclosegcode_Click);
            // 
            // toolStripButtontool
            // 
            this.toolStripButtontool.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtontool.Image = global::ASVCNC.Properties.Resources.toolbox;
            this.toolStripButtontool.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtontool.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtontool.Margin = new System.Windows.Forms.Padding(10, 1, 0, 2);
            this.toolStripButtontool.Name = "toolStripButtontool";
            this.toolStripButtontool.Size = new System.Drawing.Size(52, 52);
            this.toolStripButtontool.Text = "Редактировать таблицу инструментов";
            this.toolStripButtontool.Click += new System.EventHandler(this.toolStripButtontool_Click);
            // 
            // toolStripButtonrun
            // 
            this.toolStripButtonrun.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonrun.Image = global::ASVCNC.Properties.Resources.tc_run;
            this.toolStripButtonrun.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonrun.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonrun.Margin = new System.Windows.Forms.Padding(10, 1, 0, 2);
            this.toolStripButtonrun.Name = "toolStripButtonrun";
            this.toolStripButtonrun.Size = new System.Drawing.Size(52, 52);
            this.toolStripButtonrun.Text = "Старт";
            this.toolStripButtonrun.Click += new System.EventHandler(this.toolStripButtonrun_Click);
            // 
            // toolStripButtonsteprun
            // 
            this.toolStripButtonsteprun.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonsteprun.Image = global::ASVCNC.Properties.Resources.gcode_pause;
            this.toolStripButtonsteprun.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonsteprun.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonsteprun.Margin = new System.Windows.Forms.Padding(10, 1, 0, 2);
            this.toolStripButtonsteprun.Name = "toolStripButtonsteprun";
            this.toolStripButtonsteprun.Size = new System.Drawing.Size(52, 52);
            this.toolStripButtonsteprun.Text = "Выполнить по шагам";
            this.toolStripButtonsteprun.Click += new System.EventHandler(this.toolStripButtonsteprun_Click);
            // 
            // toolStripButtonstop
            // 
            this.toolStripButtonstop.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonstop.Image = global::ASVCNC.Properties.Resources.stop;
            this.toolStripButtonstop.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonstop.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonstop.Margin = new System.Windows.Forms.Padding(10, 1, 0, 2);
            this.toolStripButtonstop.Name = "toolStripButtonstop";
            this.toolStripButtonstop.Size = new System.Drawing.Size(52, 52);
            this.toolStripButtonstop.Text = "Сбросить и остановить выполнение программы";
            this.toolStripButtonstop.Click += new System.EventHandler(this.toolStripButtonstop_Click);
            // 
            // toolStripButtonsimulat
            // 
            this.toolStripButtonsimulat.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonsimulat.Image = global::ASVCNC.Properties.Resources.debug_run;
            this.toolStripButtonsimulat.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonsimulat.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonsimulat.Margin = new System.Windows.Forms.Padding(10, 1, 0, 2);
            this.toolStripButtonsimulat.Name = "toolStripButtonsimulat";
            this.toolStripButtonsimulat.Size = new System.Drawing.Size(52, 52);
            this.toolStripButtonsimulat.Text = "Симуляция работы";
            this.toolStripButtonsimulat.Click += new System.EventHandler(this.toolStripButtonsimulat_Click);
            // 
            // toolStripButtonparamm
            // 
            this.toolStripButtonparamm.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonparamm.Image = global::ASVCNC.Properties.Resources.paramm;
            this.toolStripButtonparamm.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonparamm.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonparamm.Margin = new System.Windows.Forms.Padding(10, 1, 0, 2);
            this.toolStripButtonparamm.Name = "toolStripButtonparamm";
            this.toolStripButtonparamm.Size = new System.Drawing.Size(52, 52);
            this.toolStripButtonparamm.Text = "Параметры";
            this.toolStripButtonparamm.Click += new System.EventHandler(this.toolStripButtonparamm_Click);
            // 
            // toolStripButtonrelog
            // 
            this.toolStripButtonrelog.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonrelog.Image = global::ASVCNC.Properties.Resources.log_delete;
            this.toolStripButtonrelog.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonrelog.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonrelog.Margin = new System.Windows.Forms.Padding(10, 1, 0, 2);
            this.toolStripButtonrelog.Name = "toolStripButtonrelog";
            this.toolStripButtonrelog.Size = new System.Drawing.Size(52, 52);
            this.toolStripButtonrelog.Text = "Очистить лог";
            this.toolStripButtonrelog.Click += new System.EventHandler(this.toolStripButtonrelog_Click);
            // 
            // panelasvcnc
            // 
            this.panelasvcnc.Controls.Add(this.tabControlkinematic);
            this.panelasvcnc.Controls.Add(this.toolStripfactpol);
            this.panelasvcnc.Controls.Add(this.toolStrippositionprog);
            this.panelasvcnc.Controls.Add(this.tabControlasvcncmanualauto);
            this.panelasvcnc.Controls.Add(this.richTextBoxlogasvcnc);
            this.panelasvcnc.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelasvcnc.Location = new System.Drawing.Point(0, 55);
            this.panelasvcnc.Name = "panelasvcnc";
            this.panelasvcnc.Size = new System.Drawing.Size(1008, 652);
            this.panelasvcnc.TabIndex = 2;
            // 
            // tabControlkinematic
            // 
            this.tabControlkinematic.Controls.Add(this.tabPagexyz);
            this.tabControlkinematic.Controls.Add(this.tabPageabc);
            this.tabControlkinematic.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlkinematic.Location = new System.Drawing.Point(270, 50);
            this.tabControlkinematic.Name = "tabControlkinematic";
            this.tabControlkinematic.SelectedIndex = 0;
            this.tabControlkinematic.Size = new System.Drawing.Size(738, 455);
            this.tabControlkinematic.TabIndex = 6;
            this.tabControlkinematic.SizeChanged += new System.EventHandler(this.tabControlkinematic_SizeChanged);
            // 
            // tabPagexyz
            // 
            this.tabPagexyz.Location = new System.Drawing.Point(4, 22);
            this.tabPagexyz.Name = "tabPagexyz";
            this.tabPagexyz.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagexyz.Size = new System.Drawing.Size(730, 429);
            this.tabPagexyz.TabIndex = 0;
            this.tabPagexyz.Text = "XYZ";
            this.tabPagexyz.UseVisualStyleBackColor = true;
            // 
            // tabPageabc
            // 
            this.tabPageabc.Location = new System.Drawing.Point(4, 22);
            this.tabPageabc.Name = "tabPageabc";
            this.tabPageabc.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageabc.Size = new System.Drawing.Size(730, 429);
            this.tabPageabc.TabIndex = 1;
            this.tabPageabc.Text = "ABC";
            this.tabPageabc.UseVisualStyleBackColor = true;
            // 
            // toolStripfactpol
            // 
            this.toolStripfactpol.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabelfactpol});
            this.toolStripfactpol.Location = new System.Drawing.Point(270, 25);
            this.toolStripfactpol.Name = "toolStripfactpol";
            this.toolStripfactpol.Size = new System.Drawing.Size(738, 25);
            this.toolStripfactpol.TabIndex = 5;
            this.toolStripfactpol.Text = "Фактическое положение";
            // 
            // toolStripLabelfactpol
            // 
            this.toolStripLabelfactpol.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripLabelfactpol.ForeColor = System.Drawing.Color.Blue;
            this.toolStripLabelfactpol.Name = "toolStripLabelfactpol";
            this.toolStripLabelfactpol.Size = new System.Drawing.Size(19, 22);
            this.toolStripLabelfactpol.Text = "0";
            // 
            // toolStrippositionprog
            // 
            this.toolStrippositionprog.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripLabelpositionprog});
            this.toolStrippositionprog.Location = new System.Drawing.Point(270, 0);
            this.toolStrippositionprog.Name = "toolStrippositionprog";
            this.toolStrippositionprog.Size = new System.Drawing.Size(738, 25);
            this.toolStrippositionprog.TabIndex = 4;
            this.toolStrippositionprog.Text = "Положение по программе";
            // 
            // toolStripLabelpositionprog
            // 
            this.toolStripLabelpositionprog.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.toolStripLabelpositionprog.ForeColor = System.Drawing.Color.Blue;
            this.toolStripLabelpositionprog.Name = "toolStripLabelpositionprog";
            this.toolStripLabelpositionprog.Size = new System.Drawing.Size(19, 22);
            this.toolStripLabelpositionprog.Text = "0";
            // 
            // tabControlasvcncmanualauto
            // 
            this.tabControlasvcncmanualauto.Controls.Add(this.tabPageauto);
            this.tabControlasvcncmanualauto.Controls.Add(this.tabPagemanual);
            this.tabControlasvcncmanualauto.Dock = System.Windows.Forms.DockStyle.Left;
            this.tabControlasvcncmanualauto.Location = new System.Drawing.Point(0, 0);
            this.tabControlasvcncmanualauto.Name = "tabControlasvcncmanualauto";
            this.tabControlasvcncmanualauto.SelectedIndex = 0;
            this.tabControlasvcncmanualauto.Size = new System.Drawing.Size(270, 505);
            this.tabControlasvcncmanualauto.TabIndex = 3;
            this.tabControlasvcncmanualauto.SelectedIndexChanged += new System.EventHandler(this.tabControlasvcncmanualauto_SelectedIndexChanged);
            // 
            // tabPageauto
            // 
            this.tabPageauto.Controls.Add(this.richTextBoxGcode);
            this.tabPageauto.Location = new System.Drawing.Point(4, 22);
            this.tabPageauto.Name = "tabPageauto";
            this.tabPageauto.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageauto.Size = new System.Drawing.Size(262, 479);
            this.tabPageauto.TabIndex = 0;
            this.tabPageauto.Text = "Автоматический режим";
            this.tabPageauto.UseVisualStyleBackColor = true;
            // 
            // richTextBoxGcode
            // 
            this.richTextBoxGcode.ContextMenuStrip = this.contextMenuStripgcode;
            this.richTextBoxGcode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBoxGcode.Location = new System.Drawing.Point(3, 3);
            this.richTextBoxGcode.Name = "richTextBoxGcode";
            this.richTextBoxGcode.ReadOnly = true;
            this.richTextBoxGcode.Size = new System.Drawing.Size(256, 473);
            this.richTextBoxGcode.TabIndex = 1;
            this.richTextBoxGcode.Text = "";
            this.richTextBoxGcode.MouseClick += new System.Windows.Forms.MouseEventHandler(this.richTextBoxGcode_MouseClick);
            // 
            // contextMenuStripgcode
            // 
            this.contextMenuStripgcode.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemrunthis});
            this.contextMenuStripgcode.Name = "contextMenuStripgcode";
            this.contextMenuStripgcode.Size = new System.Drawing.Size(180, 26);
            // 
            // toolStripMenuItemrunthis
            // 
            this.toolStripMenuItemrunthis.Name = "toolStripMenuItemrunthis";
            this.toolStripMenuItemrunthis.Size = new System.Drawing.Size(179, 22);
            this.toolStripMenuItemrunthis.Text = "Выполнить отсюда";
            this.toolStripMenuItemrunthis.Click += new System.EventHandler(this.toolStripMenuItemrunthis_Click);
            // 
            // tabPagemanual
            // 
            this.tabPagemanual.Controls.Add(this.labelregs);
            this.tabPagemanual.Controls.Add(this.trackBarregs);
            this.tabPagemanual.Controls.Add(this.labelregf);
            this.tabPagemanual.Controls.Add(this.trackBarfscal);
            this.tabPagemanual.Controls.Add(this.buttonrun);
            this.tabPagemanual.Controls.Add(this.labelcmd);
            this.tabPagemanual.Controls.Add(this.textBoxcmd);
            this.tabPagemanual.Location = new System.Drawing.Point(4, 22);
            this.tabPagemanual.Name = "tabPagemanual";
            this.tabPagemanual.Padding = new System.Windows.Forms.Padding(3);
            this.tabPagemanual.Size = new System.Drawing.Size(262, 479);
            this.tabPagemanual.TabIndex = 1;
            this.tabPagemanual.Text = "Ручной режим";
            this.tabPagemanual.UseVisualStyleBackColor = true;
            // 
            // labelregs
            // 
            this.labelregs.AutoSize = true;
            this.labelregs.Location = new System.Drawing.Point(8, 348);
            this.labelregs.Name = "labelregs";
            this.labelregs.Size = new System.Drawing.Size(122, 13);
            this.labelregs.TabIndex = 7;
            this.labelregs.Text = "Регулировка оборотов";
            // 
            // trackBarregs
            // 
            this.trackBarregs.Location = new System.Drawing.Point(8, 364);
            this.trackBarregs.Maximum = 150;
            this.trackBarregs.Minimum = 1;
            this.trackBarregs.Name = "trackBarregs";
            this.trackBarregs.Size = new System.Drawing.Size(247, 45);
            this.trackBarregs.TabIndex = 8;
            this.trackBarregs.Value = 100;
            this.trackBarregs.ValueChanged += new System.EventHandler(this.trackBarregs_ValueChanged);
            // 
            // labelregf
            // 
            this.labelregf.AutoSize = true;
            this.labelregf.Location = new System.Drawing.Point(8, 412);
            this.labelregf.Name = "labelregf";
            this.labelregf.Size = new System.Drawing.Size(110, 13);
            this.labelregf.TabIndex = 6;
            this.labelregf.Text = "Регулировка подачи";
            // 
            // trackBarfscal
            // 
            this.trackBarfscal.Location = new System.Drawing.Point(8, 428);
            this.trackBarfscal.Maximum = 150;
            this.trackBarfscal.Minimum = 1;
            this.trackBarfscal.Name = "trackBarfscal";
            this.trackBarfscal.Size = new System.Drawing.Size(247, 45);
            this.trackBarfscal.TabIndex = 6;
            this.trackBarfscal.Value = 100;
            this.trackBarfscal.ValueChanged += new System.EventHandler(this.trackBarfscal_ValueChanged);
            // 
            // buttonrun
            // 
            this.buttonrun.Location = new System.Drawing.Point(9, 73);
            this.buttonrun.Name = "buttonrun";
            this.buttonrun.Size = new System.Drawing.Size(247, 23);
            this.buttonrun.TabIndex = 4;
            this.buttonrun.Text = "Выполнить";
            this.buttonrun.UseVisualStyleBackColor = true;
            this.buttonrun.Click += new System.EventHandler(this.buttonrun_Click);
            // 
            // labelcmd
            // 
            this.labelcmd.AutoSize = true;
            this.labelcmd.Location = new System.Drawing.Point(6, 13);
            this.labelcmd.Name = "labelcmd";
            this.labelcmd.Size = new System.Drawing.Size(102, 13);
            this.labelcmd.TabIndex = 4;
            this.labelcmd.Text = "Командная строка";
            // 
            // textBoxcmd
            // 
            this.textBoxcmd.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.textBoxcmd.Location = new System.Drawing.Point(8, 29);
            this.textBoxcmd.Name = "textBoxcmd";
            this.textBoxcmd.Size = new System.Drawing.Size(248, 38);
            this.textBoxcmd.TabIndex = 4;
            this.textBoxcmd.TextChanged += new System.EventHandler(this.textBoxcmd_TextChanged);
            // 
            // richTextBoxlogasvcnc
            // 
            this.richTextBoxlogasvcnc.ContextMenuStrip = this.contextMenuStripasvcnclog;
            this.richTextBoxlogasvcnc.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.richTextBoxlogasvcnc.Location = new System.Drawing.Point(0, 505);
            this.richTextBoxlogasvcnc.Name = "richTextBoxlogasvcnc";
            this.richTextBoxlogasvcnc.ReadOnly = true;
            this.richTextBoxlogasvcnc.Size = new System.Drawing.Size(1008, 147);
            this.richTextBoxlogasvcnc.TabIndex = 0;
            this.richTextBoxlogasvcnc.Text = "";
            // 
            // contextMenuStripasvcnclog
            // 
            this.contextMenuStripasvcnclog.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItemlogclear});
            this.contextMenuStripasvcnclog.Name = "contextMenuStripasvcnclog";
            this.contextMenuStripasvcnclog.Size = new System.Drawing.Size(181, 26);
            // 
            // toolStripMenuItemlogclear
            // 
            this.toolStripMenuItemlogclear.Name = "toolStripMenuItemlogclear";
            this.toolStripMenuItemlogclear.Size = new System.Drawing.Size(180, 22);
            this.toolStripMenuItemlogclear.Text = "Удалить все записи";
            this.toolStripMenuItemlogclear.Click += new System.EventHandler(this.toolStripMenuItemlogclear_Click);
            // 
            // checkBoxlog
            // 
            this.checkBoxlog.AutoSize = true;
            this.checkBoxlog.Location = new System.Drawing.Point(734, 12);
            this.checkBoxlog.Name = "checkBoxlog";
            this.checkBoxlog.Size = new System.Drawing.Size(252, 17);
            this.checkBoxlog.TabIndex = 3;
            this.checkBoxlog.Text = "Выводить в лог команды от интерпретатора";
            this.checkBoxlog.UseVisualStyleBackColor = true;
            // 
            // toolStripButtonabout
            // 
            this.toolStripButtonabout.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonabout.Image = global::ASVCNC.Properties.Resources.about;
            this.toolStripButtonabout.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonabout.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonabout.Margin = new System.Windows.Forms.Padding(10, 1, 0, 2);
            this.toolStripButtonabout.Name = "toolStripButtonabout";
            this.toolStripButtonabout.Size = new System.Drawing.Size(52, 52);
            this.toolStripButtonabout.Text = "Информация";
            this.toolStripButtonabout.Click += new System.EventHandler(this.toolStripButtonabout_Click);
            // 
            // ASVCNC
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1008, 729);
            this.Controls.Add(this.checkBoxlog);
            this.Controls.Add(this.panelasvcnc);
            this.Controls.Add(this.toolStripasvcnc);
            this.Controls.Add(this.statusStripasvcnc);
            this.Name = "ASVCNC";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ASVCNC";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.ASVCNC_FormClosing);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.statusStripasvcnc.ResumeLayout(false);
            this.statusStripasvcnc.PerformLayout();
            this.toolStripasvcnc.ResumeLayout(false);
            this.toolStripasvcnc.PerformLayout();
            this.panelasvcnc.ResumeLayout(false);
            this.panelasvcnc.PerformLayout();
            this.tabControlkinematic.ResumeLayout(false);
            this.toolStripfactpol.ResumeLayout(false);
            this.toolStripfactpol.PerformLayout();
            this.toolStrippositionprog.ResumeLayout(false);
            this.toolStrippositionprog.PerformLayout();
            this.tabControlasvcncmanualauto.ResumeLayout(false);
            this.tabPageauto.ResumeLayout(false);
            this.contextMenuStripgcode.ResumeLayout(false);
            this.tabPagemanual.ResumeLayout(false);
            this.tabPagemanual.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarregs)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarfscal)).EndInit();
            this.contextMenuStripasvcnclog.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.StatusStrip statusStripasvcnc;
        private System.Windows.Forms.ToolStrip toolStripasvcnc;
        private System.Windows.Forms.Panel panelasvcnc;
        private System.Windows.Forms.RichTextBox richTextBoxlogasvcnc;
        private System.Windows.Forms.ToolStripButton toolStripButtonopengcod;
        private System.Windows.Forms.ToolStripButton toolStripButtonclosegcode;
        private System.Windows.Forms.ToolStripButton toolStripButtontool;
        private System.Windows.Forms.ToolStripButton toolStripButtonrelog;
        private System.Windows.Forms.RichTextBox richTextBoxGcode;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelcountstrasvcnc;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelcountgcodstr;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripasvcnclog;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemlogclear;
        private System.Windows.Forms.ToolStripButton toolStripButtoneditgcode;
        private System.Windows.Forms.ContextMenuStrip contextMenuStripgcode;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItemrunthis;
        private System.Windows.Forms.ToolStripButton toolStripButtonrun;
        private System.Windows.Forms.ToolStripButton toolStripButtonsteprun;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelselectgcode;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelnumselectstr;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelgmsfcodes;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelgmsfcod;
        private System.Windows.Forms.TabControl tabControlasvcncmanualauto;
        private System.Windows.Forms.TabPage tabPageauto;
        private System.Windows.Forms.TabPage tabPagemanual;
        private System.Windows.Forms.Button buttonrun;
        private System.Windows.Forms.Label labelcmd;
        private System.Windows.Forms.TextBox textBoxcmd;
        private System.Windows.Forms.ToolStripButton toolStripButtonsimulat;
        private System.Windows.Forms.ToolStripButton toolStripButtonparamm;
        private System.Windows.Forms.ToolStrip toolStrippositionprog;
        private System.Windows.Forms.ToolStripLabel toolStripLabelpositionprog;
        private System.Windows.Forms.ToolStrip toolStripfactpol;
        private System.Windows.Forms.ToolStripLabel toolStripLabelfactpol;
        private System.Windows.Forms.Label labelregf;
        private System.Windows.Forms.TrackBar trackBarfscal;
        private System.Windows.Forms.Label labelregs;
        private System.Windows.Forms.TrackBar trackBarregs;
        private System.Windows.Forms.CheckBox checkBoxlog;
        private System.Windows.Forms.ToolStripButton toolStripButtonstop;
        private System.Windows.Forms.TabControl tabControlkinematic;
        private System.Windows.Forms.TabPage tabPagexyz;
        private System.Windows.Forms.TabPage tabPageabc;
        private System.Windows.Forms.ToolStripButton toolStripButtonabout;

    }
}

