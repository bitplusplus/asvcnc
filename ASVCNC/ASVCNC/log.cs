﻿using System;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Reflection;

namespace ASVCNC
{
    public static class logwrite 
    {      

        public static RichTextBox OutputBox ;


        public static void savelog(string nam)
        {
            OutputBox.SaveFile(nam, RichTextBoxStreamType.PlainText);
        }


        // Generic Write Method

        public static void  Write(string text)
        {
            if (!OutputBox.IsDisposed)
            {
                Action f = () =>
                {
                    OutputBox.AppendText(String.Format("[{0:d/M/yyyy HH:mm:ss}] {1}", DateTime.Now, text) + "\n");
                    OutputBox.ScrollToCaret();
                };

                if (OutputBox.InvokeRequired)
                    // Не ждем завершение UI операции.
                    OutputBox.BeginInvoke(f);
                else
                    f();
            }
            
        }

        public static void LoadLog(string nam)
        {
            if (File.Exists(nam))
            {
                OutputBox.LoadFile(nam, RichTextBoxStreamType.PlainText);
            }

        }
    }
}


