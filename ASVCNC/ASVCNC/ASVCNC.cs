﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using InterpreterG;
using System.IO;
using System.Diagnostics;
using ASVCNCPARAMETRMASHINE;

namespace ASVCNC
{
    public partial class ASVCNC : Form
    {
        public bool steprun;
        public bool executecommand;
        public int numstrgcodeexecute;
        bool openfile;        
        String logpath;
        RS274 interpretergcode;
        Machine machine;
        int countstrgcode;
        ParametrMachine winsetparam;
        private WindowGraph wingrxyz;
        private WindowGraph wingrabc;

        private double scalexyz = 0;
        private double scaleabc = 0;

        public ASVCNC()
        {
            InitializeComponent();
            winsetparam = new ParametrMachine();
            wingrxyz = new WindowGraph();
            wingrabc = new WindowGraph();
        }
        ~ASVCNC()
        {           

        }
        //-----------------------------------------------------------------------------------//
        private void Form1_Load(object sender, EventArgs e)
        {
            wingrxyz.Size = tabPagexyz.Size;
            wingrabc.Size = tabPageabc.Size;
            tabPagexyz.Controls.Add(wingrxyz);
            tabPageabc.Controls.Add(wingrabc);

            logpath = Directory.GetCurrentDirectory() +"\\"+ Properties.Settings.Default.namelogfile;
            logwrite.OutputBox = richTextBoxlogasvcnc;
            interpretergcode = new RS274();
            InterpreterG.RS274.prinevent += RS274_prinevent;
            InterpreterG.RS274.evenerrmes += RS274_evenerrmes;
            logwrite.LoadLog(logpath);            
            machine = new Machine();
            machine.nexcmd += machine_nexcmd;
            machine.ubdguiposition += machine_ubdguiposition;
            countstrgcode = 0;
            steprun = false;
            executecommand = false;
            numstrgcodeexecute = 0;
            openfile = false;
            machine.bckwork.RunWorkerAsync();            
            getgmsfcode();
            interpretergcode.rs274ngc_resetV();
            interpretergcode.rs274ngc_initV();

        }
        //-----------------------------------------------------------------------------------//
        void machine_ubdguiposition(StringBuilder strbr)
        {
            toolStripLabelfactpol.Text = strbr.ToString();
            
        } 
        //-----------------------------------------------------------------------------------//
        void RS274_evenerrmes(string A_0)
        {
            logwrite.Write(" Ошибка :" + A_0 + Environment.NewLine);
        }
        //-----------------------------------------------------------------------------------//
        string comman ="";
        void RS274_prinevent(string A_0)
        {
            comman += A_0;
            if ((comman.Substring(comman.Length - 1)) == "\n")
            {
               
                if(checkBoxlog.Checked) logwrite.Write(" Команда :" + comman + Environment.NewLine);
                machine.interpretertexttofunction(comman);
                comman = comman.Remove(0);
            }
            

        }
        //-----------------------------------------------------------------------------------//
        private void toolStripButtonopengcod_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialoggcode = new OpenFileDialog();
            int openstatus;
            int stat = ReturnStatus.sRS274NGC_OK;
            int statexec = ReturnStatus.sRS274NGC_OK; ;
            double[] curentposition = new double[6];            

            openFileDialoggcode.InitialDirectory = "c:\\";
            openFileDialoggcode.Filter = "CNC files (*.cnc)|*.cnc|All files (*.*)|*.*";
            openFileDialoggcode.FilterIndex = 2;
            openFileDialoggcode.RestoreDirectory = true;

            if (openFileDialoggcode.ShowDialog() == DialogResult.OK)
            {
                openstatus = interpretergcode.rs274ngc_openV(openFileDialoggcode.FileName);
                if (openstatus == ReturnStatus.sRS274NGC_OK)
                {
                    logwrite.Write(" Файл открыт. Отлично " + Environment.NewLine);                    
                    openfile = true;

                    richTextBoxGcode.Clear();
                    richTextBoxGcode.LoadFile(openFileDialoggcode.FileName, RichTextBoxStreamType.PlainText);
                    
                    stat = ReturnStatus.sRS274NGC_OK;
                    statexec = 0;
                    for (int ig = 0; (ig < 99999) && (ReturnStatus.sRS274NGC_OK == stat); ig++)
                    {
                        stat = interpretergcode.rs274ngc_readV(null);
                        statexec++;
                    }
                    toolStripStatusLabelcountgcodstr.Text = statexec.ToString();
                    countstrgcode = statexec;
                    if (stat>3)interpretergcode.rs274ngc_report_errorV(stat, 0);

                    List<double> listpositionx = new List<double>(countstrgcode+10);
                    List<double> listpositiony = new List<double>(countstrgcode + 10);
                    List<double> listpositionz = new List<double>(countstrgcode + 10);
                    List<double> listpositiona = new List<double>(countstrgcode + 10);
                    List<double> listpositionb = new List<double>(countstrgcode + 10);
                    List<double> listpositionc = new List<double>(countstrgcode + 10);

                    stat = ReturnStatus.sRS274NGC_OK;
                    statexec = ReturnStatus.sRS274NGC_OK;
                    interpretergcode.rs274ngc_nc_set(0);
                    for (int ig = 0; (ig < 99999) && (ReturnStatus.sRS274NGC_OK == stat) && (statexec == ReturnStatus.sRS274NGC_OK) && (ig<= countstrgcode); ig++)
                    {
                        stat = interpretergcode.rs274ngc_readV(null);
                        statexec = interpretergcode.rs274ngc_executeV();
                        interpretergcode.rs274ngc_getcurrentposition(curentposition);
                        listpositionx.Add(curentposition[0]);
                        listpositiony.Add(curentposition[1]);
                        listpositionz.Add(curentposition[2]);
                        listpositiona.Add(curentposition[3]);
                        listpositionb.Add(curentposition[4]);
                        listpositionc.Add(curentposition[5]);
                    }
                    if (statexec > 3) interpretergcode.rs274ngc_report_errorV(statexec, 0);
                    logwrite.Write(" Разобрано строк Gcode :" + interpretergcode.rs274ngc_sequence_numberV().ToString() + " Отлично " + Environment.NewLine);

                    double[] xyzmaxmin = new double[6];
                    xyzmaxmin[0] = listpositionx.Max();
                    xyzmaxmin[1] = listpositionx.Min();
                    xyzmaxmin[2] = listpositiony.Max();
                    xyzmaxmin[3] = listpositiony.Min();
                    xyzmaxmin[4] = listpositionz.Max();
                    xyzmaxmin[5] = listpositionz.Min();

                    double[] abcmaxmin = new double[6];
                    abcmaxmin[0] = listpositiona.Max();
                    abcmaxmin[1]  = listpositiona.Min();
                    abcmaxmin[2]  = listpositionb.Max();
                    abcmaxmin[3]  = listpositionb.Min();
                    abcmaxmin[4] = listpositionc.Max();
                    abcmaxmin[5] = listpositionc.Min();

                    double predelxyz = Math.Abs(xyzmaxmin.Min() - xyzmaxmin.Max());
                    double predelabc = Math.Abs(abcmaxmin.Min() - abcmaxmin.Max());

                    scalexyz = 0.5 / predelxyz;
                    scaleabc = 0.5 / predelabc;
                    formprogress frmprg = new formprogress();
                    frmprg.progressBarprocess.Minimum = 0;
                    frmprg.progressBarprocess.Maximum = countstrgcode;
                    frmprg.Show();
                    stat = ReturnStatus.sRS274NGC_OK;
                    statexec = ReturnStatus.sRS274NGC_OK;
                    interpretergcode.rs274ngc_nc_set(0);
                    for (int ig = 0; (ig < 99999) && (ReturnStatus.sRS274NGC_OK == stat) && (statexec == ReturnStatus.sRS274NGC_OK) && (ig <= countstrgcode); ig++)
                    {
                        stat = interpretergcode.rs274ngc_readV(null);
                        statexec = interpretergcode.rs274ngc_executeV();
                        interpretergcode.rs274ngc_getcurrentposition(curentposition);

                        wingrxyz.addpoint3d_(curentposition[0] * scalexyz, curentposition[1] * scalexyz, curentposition[2] * scalexyz,Color.Gold);
                        wingrabc.addpoint3d_(curentposition[3] * scaleabc, curentposition[4] * scaleabc, curentposition[5] * scaleabc, Color.Gold);
                        frmprg.progressBarprocess.Value += 1;
    
                    }

                    if (frmprg!= null)frmprg.Close();
                    getgmsfcode();
                    interpretergcode.rs274ngc_nc_set(0);
                    selectlinigcode(0);
                    tabControlasvcncmanualauto.SelectedIndex = 0;
                   
                }
                else
                {
                    interpretergcode.rs274ngc_report_errorV(openstatus, 0);
                }

            }

        }
        //-----------------------------------------------------------------------------------//
        private void selectlinigcode(int line)
        {

            Dispatcher.Invoke(this, () =>
            {

                int indexcharfer = richTextBoxGcode.GetFirstCharIndexFromLine(line);
                int linelenght = richTextBoxGcode.Lines[line].Length;

                toolStripStatusLabelnumselectstr.Text = line.ToString();

                richTextBoxGcode.DeselectAll();
                richTextBoxGcode.SelectionStart = indexcharfer;
                richTextBoxGcode.SelectionLength = linelenght;
                richTextBoxGcode.HideSelection = false;
                richTextBoxGcode.Select(indexcharfer, linelenght);

                //richTextBoxGcode.ScrollToCaret();
                richTextBoxGcode.Focus();  


            });
        }
        //-----------------------------------------------------------------------------------//
        private void toolStripButtonclosegcode_Click(object sender, EventArgs e)
        {

            if (machine.statusexecute == 0)
            {
                int stat = interpretergcode.rs274ngc_closeV();
                openfile = false;
                if (stat == ReturnStatus.sRS274NGC_OK)
                {
                    logwrite.Write(" Файл Закрыт. Отлично " + Environment.NewLine);
                    richTextBoxGcode.Clear();
                }
                else
                {
                    interpretergcode.rs274ngc_report_errorV(stat, 0);
                }
            }
        }
        //-----------------------------------------------------------------------------------//
        private void ASVCNC_FormClosing(object sender, FormClosingEventArgs e)
        {
            logwrite.Write(" Завершение работы. Отлично " + Environment.NewLine);
            logwrite.savelog(logpath);
        }
        //-----------------------------------------------------------------------------------//
        private void toolStripButtontool_Click(object sender, EventArgs e)
        {
           
            OpenFileDialog openFileDialoggcode = new OpenFileDialog();

            openFileDialoggcode.InitialDirectory = "c:\\";
            openFileDialoggcode.Filter = "tools files (*.cnc)|*.cnc|All files (*.*)|*.*";
            openFileDialoggcode.FilterIndex = 2;
            openFileDialoggcode.RestoreDirectory = true;

            if (openFileDialoggcode.ShowDialog() == DialogResult.OK)
            {
                interpretergcode.rs274ngc_edittoolsV(openFileDialoggcode.FileName);
                
            }
            
        }
        //-----------------------------------------------------------------------------------//
        private void toolStripButtonrelog_Click(object sender, EventArgs e)
        {
            if (!executecommand)
            {
                if (MessageBox.Show("Стереть все записи в журнале лога ?", "Вопрос", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    richTextBoxlogasvcnc.Clear();
                }
            }else logwrite.Write(" остановите программу, после можно будет очистить лог  " + Environment.NewLine);
           

        }
        //-----------------------------------------------------------------------------------//
        private void toolStripMenuItemlogclear_Click(object sender, EventArgs e)
        {
            if (!executecommand)
            {
                if (MessageBox.Show("Стереть все записи в журнале лога ?", "Вопрос", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    richTextBoxlogasvcnc.Clear();
                }
            }
            else logwrite.Write(" остановите программу, после можно будет очистить лог  " + Environment.NewLine);

            
        }
        //-----------------------------------------------------------------------------------//
        private void toolStripButtoneditgcode_Click(object sender, EventArgs e)
        {

            string nameopen = interpretergcode.rs274ngc_file_nameV(255);
            if (nameopen != string.Empty)
            {
                Process.Start("notepad.exe", nameopen);
                logwrite.Write(" Редактирование файла. Отлично " + Environment.NewLine);
            }
            else
            {
                logwrite.Write(" Файл не открыт " + Environment.NewLine);
            }
            
        }
        //-----------------------------------------------------------------------------------//
        int curline = 0;
        private void richTextBoxGcode_MouseClick(object sender, MouseEventArgs e)
        {
            if(e.Button == MouseButtons.Left)
            {
                int i = richTextBoxGcode.GetLineFromCharIndex(richTextBoxGcode.GetCharIndexFromPosition(e.Location));
            
                if (i < richTextBoxGcode.Lines.Length)
                {
                  curline =richTextBoxGcode.GetLineFromCharIndex(richTextBoxGcode.GetCharIndexFromPosition(e.Location));                
                  richTextBoxGcode.Select(richTextBoxGcode.GetFirstCharIndexFromLine(curline), richTextBoxGcode.Lines[curline].Length);
                  richTextBoxGcode.Focus();
                  toolStripStatusLabelnumselectstr.Text = curline.ToString();               
           
                }
            }
        }
        //-----------------------------------------------------------------------------------//
        private void toolStripButtonsteprun_Click(object sender, EventArgs e)
        {
            if (openfile == true)
            if(steprun == false)
            {
                steprun = true;
                toolStripButtonsteprun.BackColor = Color.Yellow;
                if (executecommand)
                {
                    executecommand = false;
                    toolStripButtonrun.BackColor = SystemColors.Control;
                }
            }
            else
            {                
                steprun = false;
                toolStripButtonsteprun.BackColor = SystemColors.Control;
            }
            else logwrite.Write(" Файл не открыт " + Environment.NewLine);
        }
        //-----------------------------------------------------------------------------------//
        private void toolStripButtonrun_Click(object sender, EventArgs e)
        {
            if (machine.statusexecute == 0)
            {
                if (openfile == true)

                    if ((executecommand == false) && (tabControlasvcncmanualauto.SelectedIndex == 0))
                    {
                        executecommand = true;
                        toolStripButtonrun.BackColor = Color.Yellow;
                        runprogramm();
                    }
                    else
                    {
                        executecommand = false;
                        toolStripButtonrun.BackColor = SystemColors.Control;

                    }
                else logwrite.Write(" Файл не открыт " + Environment.NewLine);
            }
            else logwrite.Write(" Остановите программу вначале " + Environment.NewLine);
        }
        //-----------------------------------------------------------------------------------//
        public void endexecutegcode()
        {
            Dispatcher.Invoke(this, () =>
            {
                executecommand = false;
                toolStripButtonrun.BackColor = SystemColors.Control;
            });

            machine.flaggcodefileend = true;
        }
        //-----------------------------------------------------------------------------------//
        private void toolStripMenuItemrunthis_Click(object sender, EventArgs e)
        {
            numstrgcodeexecute = curline;
            interpretergcode.rs274ngc_nc_set(numstrgcodeexecute);
        }
        //-----------------------------------------------------------------------------------//
        public void getgmsfcode()
        {
            int[] gcodes = new int[12];
            int[] mcodes = new int[7];
            double[] sett = new double[3];
            int[] tolslot = new int[3];
            double[] curentposition = new double[6];

            Dispatcher.Invoke(this, () =>
            {


                interpretergcode.rs274ngc_active_g_codesV(gcodes);
                interpretergcode.rs274ngc_active_m_codesV(mcodes);
                interpretergcode.rs274ngc_active_settingsV(sett);
                interpretergcode.rs274ngc_getcurrenttollslot(tolslot);
                interpretergcode.rs274ngc_getcurrentposition(curentposition);

                toolStripStatusLabelgmsfcod.Text = "";

                for (int i = 1; i < 12; i++)
                {
                    if (gcodes[i] >= 0)
                    {
                        toolStripStatusLabelgmsfcod.Text += " G" + (gcodes[i] / 10).ToString();
                    }
                }
                toolStripStatusLabelgmsfcod.Text += " / ";
                for (int i = 1; i < 7; i++)
                {
                    if (mcodes[i] >= 0)
                    {
                        toolStripStatusLabelgmsfcod.Text += " M" + mcodes[i].ToString();
                    }
                }
                toolStripStatusLabelgmsfcod.Text += " / ";

                if (sett[0] >= 0)
                {
                    // toolStripStatusLabelgmsfcod.Text += " r" + sett[0].ToString();
                }
                if (sett[1] >= 0)
                {
                    toolStripStatusLabelgmsfcod.Text += " F" + (sett[1]*machine.fscal).ToString();
                }
                if (sett[2] >= 0)
                {
                    toolStripStatusLabelgmsfcod.Text += " S" + (sett[2]*machine.sscal).ToString();
                }

                toolStripStatusLabelgmsfcod.Text += " / ";
                toolStripStatusLabelgmsfcod.Text += " T" + tolslot[1].ToString();
                toolStripStatusLabelgmsfcod.Text += " SLOT" + tolslot[0].ToString();

                StringBuilder strb = new StringBuilder();
                strb.Append("X = (" + String.Format("{0:0000.000}", curentposition[0]) + ") ");
                strb.Append("Y = (" + String.Format("{0:0000.000}", curentposition[1]) + ") ");
                strb.Append("Z = (" + String.Format("{0:0000.000}", curentposition[2]) + ") ");
                strb.Append("A = (" + String.Format("{0:0000.000}", curentposition[3]) + ") ");
                strb.Append("B = (" + String.Format("{0:0000.000}", curentposition[4]) + ") ");
                strb.Append("C = (" + String.Format("{0:0000.000}", curentposition[5]) + ") ");

                toolStripLabelpositionprog.Text = strb.ToString();

                wingrxyz.addpoint3d_(curentposition[0] * scalexyz, curentposition[1] * scalexyz, curentposition[2] * scalexyz, Color.Red);
                wingrabc.addpoint3d_(curentposition[3] * scaleabc, curentposition[4] * scaleabc, curentposition[4] * scaleabc, Color.Red);
                
               
            });           

        }
        //-----------------------------------------------------------------------------------//
        private void buttonrun_Click(object sender, EventArgs e)
        {
            int stat = interpretergcode.rs274ngc_readV(textBoxcmd.Text);

            if(ReturnStatus.sRS274NGC_OK == stat)
            {
                stat = interpretergcode.rs274ngc_executeV();
                if (ReturnStatus.sRS274NGC_OK != stat)
                {
                    interpretergcode.rs274ngc_report_errorV(stat, 0);
                }
            }
            else
            {
                interpretergcode.rs274ngc_report_errorV(stat, 0);
            }
            getgmsfcode();
        }
        //-----------------------------------------------------------------------------------//
        private void tabControlasvcncmanualauto_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (((TabControl)sender).SelectedIndex > 0)
            {
                endexecutegcode();
            }
        }
        //-----------------------------------------------------------------------------------//
        private void textBoxcmd_TextChanged(object sender, EventArgs e)
        {
            textBoxcmd.Text= textBoxcmd.Text.ToUpper();
            textBoxcmd.SelectionStart = textBoxcmd.Text.Length;
        }
        //-----------------------------------------------------------------------------------//
       private void runprogramm()
        {
            int stat = 0;
            int squncenum = 0;    

            if ((steprun==true) &&(executecommand == true))
            {
                
                
                stat = interpretergcode.rs274ngc_readV(null);
                if (ReturnStatus.sRS274NGC_OK != stat)
                {
                    interpretergcode.rs274ngc_report_errorV(stat, 0);
                }
                stat = interpretergcode.rs274ngc_executeV();
                if (ReturnStatus.sRS274NGC_OK != stat)
                {
                    interpretergcode.rs274ngc_report_errorV(stat, 0);
                }
                squncenum = interpretergcode.rs274ngc_sequence_numberV();
                getgmsfcode();
                selectlinigcode(squncenum);

                endexecutegcode();
            }
            else if (executecommand == true)
            {
                machine.statusexecute = 5;
                machine.flaggcodefileend = false;
                
            }

        }
        //-----------------------------------------------------------------------------------//
       int readstat = 0;
       int executestatus = 0;
       int numstrgcod = 0;
       private void runavtomat()
       {


           if ((executecommand) && (!steprun))
           {

               readstat = interpretergcode.rs274ngc_readV(null);
               executestatus = interpretergcode.rs274ngc_executeV();
               numstrgcod = interpretergcode.rs274ngc_sequence_numberV();
               selectlinigcode(numstrgcod);
               getgmsfcode();

               if ((ReturnStatus.sRS274NGC_OK == readstat) && (executestatus == ReturnStatus.sRS274NGC_OK) && (numstrgcod < 99999))
               {

               }
               else
               {
                   if (readstat > 3) interpretergcode.rs274ngc_report_errorV(readstat, 0);
                   if (executestatus > 3) interpretergcode.rs274ngc_report_errorV(executestatus, 0);

                   readstat = 0;
                   executestatus = 0;
                   numstrgcod = 0;                  
                   endexecutegcode();
               }
               machine.flaggcodereaddone = true;
           }else
           {               
               endexecutegcode();
           }


       }
       //-----------------------------------------------------------------------------------//
       private void toolStripButtonsimulat_Click(object sender, EventArgs e)
       {

           if (machine.statusexecute == 0)
           {
               if (machine.simulation)
               {
                   machine.simulation = false;
                   toolStripButtonsimulat.BackColor = SystemColors.Control;
               }
               else
               {
                   machine.simulation = true;
                   toolStripButtonsimulat.BackColor = Color.Yellow;
               }
           }
           else logwrite.Write(" Остановите программу вначале " + Environment.NewLine);
           
       }
        //-----------------------------------------------------------------------------------//
       void machine_nexcmd()
       {
           runavtomat();           
       }
       //-----------------------------------------------------------------------------------//
       private void toolStripButtonparamm_Click(object sender, EventArgs e)
       {           
           winsetparam.setparametermachine();

       }
       //-----------------------------------------------------------------------------------//
       private void trackBarregs_ValueChanged(object sender, EventArgs e)
       {
           machine.sscal =   trackBarregs.Value /100.0;
           labelregs.Text = "Регулировка оборотов " + trackBarregs.Value.ToString() + "%";
       }
       //-----------------------------------------------------------------------------------//
       private void trackBarfscal_ValueChanged(object sender, EventArgs e)
       {
           machine.fscal = trackBarfscal.Value / 100.0;
           labelregf.Text = "Регулировка подачи " + trackBarfscal.Value.ToString() + "%";
       }
       //-----------------------------------------------------------------------------------//
       private void toolStripButtonstop_Click(object sender, EventArgs e)
       {
           endexecutegcode();
           machine.statusexecute = 0;
       }
       //-----------------------------------------------------------------------------------//
       private void tabControlkinematic_SizeChanged(object sender, EventArgs e)
       {
           wingrxyz.Size = tabPagexyz.Size;
           wingrabc.Size = tabPageabc.Size;
       }

       private void toolStripButtonabout_Click(object sender, EventArgs e)
       {
           Aboutprogramm awin= new Aboutprogramm();           
           awin.Show();

       }
       //-----------------------------------------------------------------------------------//







    }//class
}//name space
