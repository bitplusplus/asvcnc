﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using InterpreterG;
using System.IO;
using System.Diagnostics;

namespace ASVCNC
{
    public delegate void AsyncAction();

    public delegate void DispatcherInvoker(Form form, AsyncAction a);

    public class Dispatcher
    {
        public static void Invoke(Form form, AsyncAction action)
        {
            if (!form.InvokeRequired)
            {
                action();
            }
            else
            {
                form.Invoke((DispatcherInvoker)Invoke, form, action);
            }
        }
    }

}
