﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ASVCNCPARAMETRMASHINE
{
    public partial class SetParameter : Form
    {        
        public SetParameter()
        {
            InitializeComponent();
        }

        private void SetParameter_Load(object sender, EventArgs e)
        {

            dataGridViewparam.Rows.Clear();
            dataGridViewparam.Rows.Add(25);

            dataGridViewparam.Rows[0].Cells[0].Value = "Максимальная подача X";
            dataGridViewparam.Rows[0].Cells[1].ValueType = typeof(double);
            dataGridViewparam.Rows[0].Cells[1].Value = Properties.Settings.Default.MaxFeedX;
            dataGridViewparam.Rows[1].Cells[0].Value = "Максимальная подача Y";
            dataGridViewparam.Rows[1].Cells[1].ValueType = typeof(double);
            dataGridViewparam.Rows[1].Cells[1].Value = Properties.Settings.Default.MaxFeedY;
            dataGridViewparam.Rows[2].Cells[0].Value = "Максимальная подача Z";
            dataGridViewparam.Rows[2].Cells[1].ValueType = typeof(double);
            dataGridViewparam.Rows[2].Cells[1].Value = Properties.Settings.Default.MaxFeedZ;
            dataGridViewparam.Rows[3].Cells[0].Value = "Максимальная подача A";
            dataGridViewparam.Rows[3].Cells[1].ValueType = typeof(double);
            dataGridViewparam.Rows[3].Cells[1].Value = Properties.Settings.Default.MaxFeedA;
            dataGridViewparam.Rows[4].Cells[0].Value = "Максимальная подача B";
            dataGridViewparam.Rows[4].Cells[1].ValueType = typeof(double);
            dataGridViewparam.Rows[4].Cells[1].Value = Properties.Settings.Default.MaxFeedB;
            dataGridViewparam.Rows[5].Cells[0].Value = "Максимальная подача C";
            dataGridViewparam.Rows[5].Cells[1].ValueType = typeof(double);
            dataGridViewparam.Rows[5].Cells[1].Value = Properties.Settings.Default.MaxFeedC;


            dataGridViewparam.Rows[6].Cells[0].Value = "Максимальная ускорение X";
            dataGridViewparam.Rows[6].Cells[1].ValueType = typeof(double);
            dataGridViewparam.Rows[6].Cells[1].Value = Properties.Settings.Default.MaxFeedRateX;
            dataGridViewparam.Rows[7].Cells[0].Value = "Максимальная ускорение Y";
            dataGridViewparam.Rows[7].Cells[1].ValueType = typeof(double);
            dataGridViewparam.Rows[7].Cells[1].Value = Properties.Settings.Default.MaxFeedRateY;
            dataGridViewparam.Rows[8].Cells[0].Value = "Максимальная ускорение Z";
            dataGridViewparam.Rows[8].Cells[1].ValueType = typeof(double);
            dataGridViewparam.Rows[8].Cells[1].Value = Properties.Settings.Default.MaxFeedRateZ;
            dataGridViewparam.Rows[9].Cells[0].Value = "Максимальная ускорение A";
            dataGridViewparam.Rows[9].Cells[1].ValueType = typeof(double);
            dataGridViewparam.Rows[9].Cells[1].Value = Properties.Settings.Default.MaxFeedRateA;
            dataGridViewparam.Rows[10].Cells[0].Value = "Максимальная ускорение B";
            dataGridViewparam.Rows[10].Cells[1].ValueType = typeof(double);
            dataGridViewparam.Rows[10].Cells[1].Value = Properties.Settings.Default.MaxFeedRateB;
            dataGridViewparam.Rows[11].Cells[0].Value = "Максимальная ускорение C";
            dataGridViewparam.Rows[11].Cells[1].ValueType = typeof(double);
            dataGridViewparam.Rows[11].Cells[1].Value = Properties.Settings.Default.MaxFeedRateC;




            dataGridViewparam.Rows[12].Cells[0].Value = "Ограничение миниму X";
            dataGridViewparam.Rows[12].Cells[1].ValueType = typeof(double);
            dataGridViewparam.Rows[12].Cells[1].Value = Properties.Settings.Default.LimitXmin;
            dataGridViewparam.Rows[13].Cells[0].Value = "Ограничение максимум X";
            dataGridViewparam.Rows[13].Cells[1].ValueType = typeof(double);
            dataGridViewparam.Rows[13].Cells[1].Value = Properties.Settings.Default.LimitXmax;


            dataGridViewparam.Rows[14].Cells[0].Value = "Ограничение миниму Y";
            dataGridViewparam.Rows[14].Cells[1].ValueType = typeof(double);
            dataGridViewparam.Rows[14].Cells[1].Value = Properties.Settings.Default.LimitYmin;
            dataGridViewparam.Rows[15].Cells[0].Value = "Ограничение максимум Y";
            dataGridViewparam.Rows[15].Cells[1].ValueType = typeof(double);
            dataGridViewparam.Rows[15].Cells[1].Value = Properties.Settings.Default.LimitYmax;

            dataGridViewparam.Rows[16].Cells[0].Value = "Ограничение миниму Z";
            dataGridViewparam.Rows[16].Cells[1].ValueType = typeof(double);
            dataGridViewparam.Rows[16].Cells[1].Value = Properties.Settings.Default.LimitZmin;
            dataGridViewparam.Rows[17].Cells[0].Value = "Ограничение максимум Z";
            dataGridViewparam.Rows[17].Cells[1].ValueType = typeof(double);
            dataGridViewparam.Rows[17].Cells[1].Value = Properties.Settings.Default.LimitZmax;

            dataGridViewparam.Rows[18].Cells[0].Value = "Ограничение миниму A";
            dataGridViewparam.Rows[18].Cells[1].ValueType = typeof(double);
            dataGridViewparam.Rows[18].Cells[1].Value = Properties.Settings.Default.LimitAmin;
            dataGridViewparam.Rows[19].Cells[0].Value = "Ограничение максимум A";
            dataGridViewparam.Rows[19].Cells[1].ValueType = typeof(double);
            dataGridViewparam.Rows[19].Cells[1].Value = Properties.Settings.Default.LimitAmax;

            dataGridViewparam.Rows[20].Cells[0].Value = "Ограничение миниму B";
            dataGridViewparam.Rows[20].Cells[1].ValueType = typeof(double);
            dataGridViewparam.Rows[20].Cells[1].Value = Properties.Settings.Default.LimitBmin;
            dataGridViewparam.Rows[21].Cells[0].Value = "Ограничение максимум B";
            dataGridViewparam.Rows[21].Cells[1].ValueType = typeof(double);
            dataGridViewparam.Rows[21].Cells[1].Value = Properties.Settings.Default.LimitBmax;

            dataGridViewparam.Rows[22].Cells[0].Value = "Ограничение миниму C";
            dataGridViewparam.Rows[22].Cells[1].ValueType = typeof(double);
            dataGridViewparam.Rows[22].Cells[1].Value = Properties.Settings.Default.LimitCmin;
            dataGridViewparam.Rows[23].Cells[0].Value = "Ограничение максимум C";
            dataGridViewparam.Rows[23].Cells[1].ValueType = typeof(double);
            dataGridViewparam.Rows[23].Cells[1].Value = Properties.Settings.Default.LimitCmax;


            dataGridViewparam.Rows[24].Cells[0].Value = "Импульс на 1мм";
            dataGridViewparam.Rows[24].Cells[1].ValueType = typeof(double);
            dataGridViewparam.Rows[24].Cells[1].Value = Properties.Settings.Default.PulseTomm;



        }


        private void toolStripButtonsave_Click(object sender, EventArgs e)
        {


            Properties.Settings.Default.MaxFeedX = Convert.ToDouble(dataGridViewparam.Rows[0].Cells[1].Value.ToString());
            Properties.Settings.Default.MaxFeedY = Convert.ToDouble(dataGridViewparam.Rows[1].Cells[1].Value.ToString());
            Properties.Settings.Default.MaxFeedZ = Convert.ToDouble(dataGridViewparam.Rows[2].Cells[1].Value.ToString());
            Properties.Settings.Default.MaxFeedA = Convert.ToDouble(dataGridViewparam.Rows[3].Cells[1].Value.ToString());
            Properties.Settings.Default.MaxFeedB = Convert.ToDouble(dataGridViewparam.Rows[4].Cells[1].Value.ToString());
            Properties.Settings.Default.MaxFeedC = Convert.ToDouble(dataGridViewparam.Rows[5].Cells[1].Value.ToString());

            Properties.Settings.Default.MaxFeedRateX = Convert.ToDouble(dataGridViewparam.Rows[6].Cells[1].Value.ToString());
            Properties.Settings.Default.MaxFeedRateY = Convert.ToDouble(dataGridViewparam.Rows[7].Cells[1].Value.ToString());
            Properties.Settings.Default.MaxFeedRateZ = Convert.ToDouble(dataGridViewparam.Rows[8].Cells[1].Value.ToString());
            Properties.Settings.Default.MaxFeedRateA = Convert.ToDouble(dataGridViewparam.Rows[9].Cells[1].Value.ToString());
            Properties.Settings.Default.MaxFeedRateB = Convert.ToDouble(dataGridViewparam.Rows[10].Cells[1].Value.ToString());
            Properties.Settings.Default.MaxFeedRateC = Convert.ToDouble(dataGridViewparam.Rows[11].Cells[1].Value.ToString());

            Properties.Settings.Default.LimitXmin = Convert.ToDouble(dataGridViewparam.Rows[12].Cells[1].Value.ToString());
            Properties.Settings.Default.LimitXmax = Convert.ToDouble(dataGridViewparam.Rows[13].Cells[1].Value.ToString());

            Properties.Settings.Default.LimitYmin = Convert.ToDouble(dataGridViewparam.Rows[14].Cells[1].Value.ToString());
            Properties.Settings.Default.LimitYmax = Convert.ToDouble(dataGridViewparam.Rows[15].Cells[1].Value.ToString());

            Properties.Settings.Default.LimitZmin = Convert.ToDouble(dataGridViewparam.Rows[16].Cells[1].Value.ToString());
            Properties.Settings.Default.LimitZmax = Convert.ToDouble(dataGridViewparam.Rows[17].Cells[1].Value.ToString());

            Properties.Settings.Default.LimitAmin = Convert.ToDouble(dataGridViewparam.Rows[18].Cells[1].Value.ToString());
            Properties.Settings.Default.LimitAmax = Convert.ToDouble(dataGridViewparam.Rows[19].Cells[1].Value.ToString());


            Properties.Settings.Default.LimitBmin = Convert.ToDouble(dataGridViewparam.Rows[20].Cells[1].Value.ToString());
            Properties.Settings.Default.LimitBmax = Convert.ToDouble(dataGridViewparam.Rows[21].Cells[1].Value.ToString());


            Properties.Settings.Default.LimitCmin = Convert.ToDouble(dataGridViewparam.Rows[22].Cells[1].Value.ToString());
            Properties.Settings.Default.LimitCmax = Convert.ToDouble(dataGridViewparam.Rows[23].Cells[1].Value.ToString());

            Properties.Settings.Default.PulseTomm = Convert.ToDouble(dataGridViewparam.Rows[24].Cells[1].Value.ToString());
            Properties.Settings.Default.Save();


        }

        int rowerr;
        private void dataGridViewparam_DataError(object sender, DataGridViewDataErrorEventArgs e)
        {
            dataGridViewparam.Rows[e.RowIndex].ErrorText = "Параметр должен иметь цифровое значение.";
            rowerr = e.RowIndex;

        }

        private void dataGridViewparam_RowValidated(object sender, DataGridViewCellEventArgs e)
        {
            dataGridViewparam.Rows[rowerr].ErrorText = "";

        }
        //----------------------------------------------------------------------------------------------------------------------//











    }
}
