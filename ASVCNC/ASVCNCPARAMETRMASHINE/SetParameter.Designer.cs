﻿namespace ASVCNCPARAMETRMASHINE
{
    partial class SetParameter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panelparamwin = new System.Windows.Forms.Panel();
            this.dataGridViewparam = new System.Windows.Forms.DataGridView();
            this.nameparametr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.valparam = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.statusStripwinmain = new System.Windows.Forms.StatusStrip();
            this.toolStripmainwin = new System.Windows.Forms.ToolStrip();
            this.toolStripButtonsave = new System.Windows.Forms.ToolStripButton();
            this.panelparamwin.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewparam)).BeginInit();
            this.toolStripmainwin.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelparamwin
            // 
            this.panelparamwin.Controls.Add(this.dataGridViewparam);
            this.panelparamwin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelparamwin.Location = new System.Drawing.Point(0, 55);
            this.panelparamwin.Name = "panelparamwin";
            this.panelparamwin.Size = new System.Drawing.Size(402, 371);
            this.panelparamwin.TabIndex = 0;
            // 
            // dataGridViewparam
            // 
            this.dataGridViewparam.AllowUserToAddRows = false;
            this.dataGridViewparam.AllowUserToDeleteRows = false;
            this.dataGridViewparam.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewparam.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.nameparametr,
            this.valparam});
            this.dataGridViewparam.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewparam.Location = new System.Drawing.Point(0, 0);
            this.dataGridViewparam.Name = "dataGridViewparam";
            this.dataGridViewparam.Size = new System.Drawing.Size(402, 371);
            this.dataGridViewparam.TabIndex = 0;
            this.dataGridViewparam.DataError += new System.Windows.Forms.DataGridViewDataErrorEventHandler(this.dataGridViewparam_DataError);
            this.dataGridViewparam.RowValidated += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridViewparam_RowValidated);
            // 
            // nameparametr
            // 
            this.nameparametr.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.nameparametr.HeaderText = "Параметр";
            this.nameparametr.MaxInputLength = 500;
            this.nameparametr.Name = "nameparametr";
            this.nameparametr.ReadOnly = true;
            this.nameparametr.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // valparam
            // 
            this.valparam.HeaderText = "Значение";
            this.valparam.MaxInputLength = 100;
            this.valparam.Name = "valparam";
            this.valparam.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.valparam.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.NotSortable;
            // 
            // statusStripwinmain
            // 
            this.statusStripwinmain.Location = new System.Drawing.Point(0, 426);
            this.statusStripwinmain.Name = "statusStripwinmain";
            this.statusStripwinmain.Size = new System.Drawing.Size(402, 22);
            this.statusStripwinmain.TabIndex = 1;
            this.statusStripwinmain.Text = "Панель статуса";
            // 
            // toolStripmainwin
            // 
            this.toolStripmainwin.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButtonsave});
            this.toolStripmainwin.Location = new System.Drawing.Point(0, 0);
            this.toolStripmainwin.Name = "toolStripmainwin";
            this.toolStripmainwin.Size = new System.Drawing.Size(402, 55);
            this.toolStripmainwin.TabIndex = 2;
            this.toolStripmainwin.Text = "Панель управления";
            // 
            // toolStripButtonsave
            // 
            this.toolStripButtonsave.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButtonsave.Image = global::ASVCNCPARAMETRMASHINE.Properties.Resources.save;
            this.toolStripButtonsave.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.toolStripButtonsave.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonsave.Name = "toolStripButtonsave";
            this.toolStripButtonsave.Size = new System.Drawing.Size(52, 52);
            this.toolStripButtonsave.Text = "Сохранить изменения в параметрах";
            this.toolStripButtonsave.Click += new System.EventHandler(this.toolStripButtonsave_Click);
            // 
            // SetParameter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(402, 448);
            this.Controls.Add(this.panelparamwin);
            this.Controls.Add(this.toolStripmainwin);
            this.Controls.Add(this.statusStripwinmain);
            this.Name = "SetParameter";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Параметры станка";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.SetParameter_Load);
            this.panelparamwin.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewparam)).EndInit();
            this.toolStripmainwin.ResumeLayout(false);
            this.toolStripmainwin.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panelparamwin;
        private System.Windows.Forms.StatusStrip statusStripwinmain;
        private System.Windows.Forms.ToolStrip toolStripmainwin;
        private System.Windows.Forms.ToolStripButton toolStripButtonsave;
        private System.Windows.Forms.DataGridView dataGridViewparam;
        private System.Windows.Forms.DataGridViewTextBoxColumn nameparametr;
        private System.Windows.Forms.DataGridViewTextBoxColumn valparam;
    }
}