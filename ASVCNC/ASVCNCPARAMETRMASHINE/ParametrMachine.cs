﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;

namespace ASVCNCPARAMETRMASHINE
{
    public class ParametrMachine
    {
        const double tiny = 1e-7;
        public ParametrMachine()
        {

        }


        public void setparametermachine()
        {

            SetParameter setparwin = new SetParameter();
            setparwin.Show();

        }



        //----------------------------------------------------------------------------------------------------------------------//
        double selectmax3val(double a, double b, double c)
        {
            double[] result = new double[3];

            result[0] = a;
            result[1] = b;
            result[2] = c;

            return result.Max();
        }
        //----------------------------------------------------------------------------------------------------------------------//
        double selectmax6val(double x, double y, double z, double a, double b, double c)
        {
            double[] result = new double[6];

            result[0] = x;
            result[1] = y;
            result[2] = z;
            result[3] = a;
            result[4] = b;
            result[5] = c;

            return result.Max();
        }

        //----------------------------------------------------------------------------------------------------------------------//
        public double getStraightVelocityfreemov(double feedcurrent, double lenght_mm_cm_inc,
            double xnew, double ynew, double znew, double anew, double bnew, double cnew,
            double xold, double yold, double zold, double aold, double bold, double cold
            )
        {
            double dx, dy, dz, da, db, dc;
            double tx, ty, tz, ta, tb, tc, tmax;
            double vel, dtot;

            bool cartesian_move = false;
            bool angular_move = false;
            /* If we get a move to nowhere (!cartesian_move && !angular_move)
               we might as well go there at the currentLinearFeedRate...
            */
            vel = feedcurrent;

            // Compute absolute travel distance for each axis:
            dx = Math.Abs(xnew - xold);
            dy = Math.Abs(ynew - yold);
            dz = Math.Abs(znew - zold);
            da = Math.Abs(anew - aold);
            db = Math.Abs(bnew - bold);
            dc = Math.Abs(cnew - cold);


            if (dx < tiny) dx = 0.0;
            if (dy < tiny) dy = 0.0;
            if (dz < tiny) dz = 0.0;
            if (da < tiny) da = 0.0;
            if (db < tiny) db = 0.0;
            if (dc < tiny) dc = 0.0;


            // Figure out what kind of move we're making:
            if (dx <= 0.0 && dy <= 0.0 && dz <= 0.0)
            {
                cartesian_move = false;
            }
            else
            {
                cartesian_move = true;
            }
            if (da <= 0.0 && db <= 0.0 && dc <= 0.0)
            {
                angular_move = false;
            }
            else
            {
                angular_move = true;
            }

            // Pure linear move:
            if (cartesian_move && !angular_move)
            {
                if (dx != 0)
                {
                    tx = Math.Abs(dx / (Properties.Settings.Default.MaxFeedX / lenght_mm_cm_inc));
                }
                else
                {
                    tx = 0.0;
                }

                if (dy != 0)
                {
                    ty = Math.Abs(dy / (Properties.Settings.Default.MaxFeedY / lenght_mm_cm_inc));
                }
                else
                {
                    ty = 0.0;
                }

                if (dz != 0)
                {
                    tz = Math.Abs(dz / (Properties.Settings.Default.MaxFeedZ / lenght_mm_cm_inc));
                }
                else
                {
                    tz = 0.0;
                }

                tmax = selectmax3val(tx, ty, tz);

                dtot = Math.Sqrt(dx * dx + dy * dy + dz * dz);

                if (tmax <= 0.0)
                {
                    vel = feedcurrent;
                }
                else
                {
                    vel = dtot / tmax;
                }
            }
            // Pure angular move:
            else if (!cartesian_move && angular_move)
            {

                if (da != 0)
                {
                    ta = Math.Abs(da / (Properties.Settings.Default.MaxFeedA / lenght_mm_cm_inc));
                }
                else
                {
                    ta = 0.0;
                }
                if (db != 0)
                {
                    tb = Math.Abs(db / (Properties.Settings.Default.MaxFeedB / lenght_mm_cm_inc));
                }
                else
                {
                    tb = 0.0;
                }
                if (dc != 0)
                {
                    tc = Math.Abs(dc / (Properties.Settings.Default.MaxFeedC / lenght_mm_cm_inc));
                }
                else
                {
                    tc = 0.0;
                }

                tmax = selectmax3val(ta, tb, tc);

                dtot = Math.Sqrt(da * da + db * db + dc * dc);
                if (tmax <= 0.0)
                {
                    vel = feedcurrent;
                }
                else
                {
                    vel = dtot / tmax;
                }
            }
            // Combination angular and linear move:
            else if (cartesian_move && angular_move)
            {



                if (dx != 0)
                {
                    tx = Math.Abs(dx / (Properties.Settings.Default.MaxFeedX / lenght_mm_cm_inc));
                }
                else
                {
                    tx = 0.0;
                }

                if (dy != 0)
                {
                    ty = Math.Abs(dy / (Properties.Settings.Default.MaxFeedY / lenght_mm_cm_inc));
                }
                else
                {
                    ty = 0.0;
                }

                if (dz != 0)
                {
                    tz = Math.Abs(dz / (Properties.Settings.Default.MaxFeedZ / lenght_mm_cm_inc));
                }
                else
                {
                    tz = 0.0;
                }

                if (da != 0)
                {
                    ta = Math.Abs(da / (Properties.Settings.Default.MaxFeedA / lenght_mm_cm_inc));
                }
                else
                {
                    ta = 0.0;
                }
                if (db != 0)
                {
                    tb = Math.Abs(db / (Properties.Settings.Default.MaxFeedB / lenght_mm_cm_inc));
                }
                else
                {
                    tb = 0.0;
                }
                if (dc != 0)
                {
                    tc = Math.Abs(dc / (Properties.Settings.Default.MaxFeedC / lenght_mm_cm_inc));
                }
                else
                {
                    tc = 0.0;
                }


                tmax = selectmax6val(tx, ty, tz, ta, tb, tc);

                dtot = Math.Sqrt(dx * dx + dy * dy + dz * dz);


                if (tmax <= 0.0)
                {
                    vel = feedcurrent;
                }
                else
                {
                    vel = dtot / tmax;
                }
            }

            return vel;
        }
        //----------------------------------------------------------------------------------------------------------------------//
        public double getStraightVelocityfeedmov(double feedcurrent, double lenght_mm_cm_inc,
            double xnew, double ynew, double znew, double anew, double bnew, double cnew,
            double xold, double yold, double zold, double aold, double bold, double cold
            )
        {
            double dx, dy, dz, da, db, dc;
            double tx, ty, tz, ta, tb, tc, tmax;
            double vel, dtot;

            bool cartesian_move = false;
            bool angular_move = false;
            /* If we get a move to nowhere (!cartesian_move && !angular_move)
               we might as well go there at the currentLinearFeedRate...
            */
            vel = feedcurrent;

            // Compute absolute travel distance for each axis:
            dx = Math.Abs(xnew - xold);
            dy = Math.Abs(ynew - yold);
            dz = Math.Abs(znew - zold);
            da = Math.Abs(anew - aold);
            db = Math.Abs(bnew - bold);
            dc = Math.Abs(cnew - cold);


            if (dx < tiny) dx = 0.0;
            if (dy < tiny) dy = 0.0;
            if (dz < tiny) dz = 0.0;
            if (da < tiny) da = 0.0;
            if (db < tiny) db = 0.0;
            if (dc < tiny) dc = 0.0;


            // Figure out what kind of move we're making:
            if (dx <= 0.0 && dy <= 0.0 && dz <= 0.0)
            {
                cartesian_move = false;
            }
            else
            {
                cartesian_move = true;
            }
            if (da <= 0.0 && db <= 0.0 && dc <= 0.0)
            {
                angular_move = false;
            }
            else
            {
                angular_move = true;
            }

            // Pure linear move:
            if (cartesian_move && !angular_move)
            {
                if (dx != 0)
                {
                    tx = Math.Abs(dx / (feedcurrent / lenght_mm_cm_inc));
                }
                else
                {
                    tx = 0.0;
                }

                if (dy != 0)
                {
                    ty = Math.Abs(dy / (feedcurrent / lenght_mm_cm_inc));
                }
                else
                {
                    ty = 0.0;
                }

                if (dz != 0)
                {
                    tz = Math.Abs(dz / (feedcurrent / lenght_mm_cm_inc));
                }
                else
                {
                    tz = 0.0;
                }

                tmax = selectmax3val(tx, ty, tz);

                dtot = Math.Sqrt(dx * dx + dy * dy + dz * dz);

                if (tmax <= 0.0)
                {
                    vel = feedcurrent;
                }
                else
                {
                    vel = dtot / tmax;
                }
            }
            // Pure angular move:
            else if (!cartesian_move && angular_move)
            {

                if (da != 0)
                {
                    ta = Math.Abs(da / (feedcurrent / lenght_mm_cm_inc));
                }
                else
                {
                    ta = 0.0;
                }
                if (db != 0)
                {
                    tb = Math.Abs(db / (feedcurrent / lenght_mm_cm_inc));
                }
                else
                {
                    tb = 0.0;
                }
                if (dc != 0)
                {
                    tc = Math.Abs(dc / (feedcurrent / lenght_mm_cm_inc));
                }
                else
                {
                    tc = 0.0;
                }

                tmax = selectmax3val(ta, tb, tc);

                dtot = Math.Sqrt(da * da + db * db + dc * dc);
                if (tmax <= 0.0)
                {
                    vel = feedcurrent;
                }
                else
                {
                    vel = dtot / tmax;
                }
            }
            // Combination angular and linear move:
            else if (cartesian_move && angular_move)
            {



                if (dx != 0)
                {
                    tx = Math.Abs(dx / (feedcurrent / lenght_mm_cm_inc));
                }
                else
                {
                    tx = 0.0;
                }

                if (dy != 0)
                {
                    ty = Math.Abs(dy / (feedcurrent / lenght_mm_cm_inc));
                }
                else
                {
                    ty = 0.0;
                }

                if (dz != 0)
                {
                    tz = Math.Abs(dz / (feedcurrent / lenght_mm_cm_inc));
                }
                else
                {
                    tz = 0.0;
                }

                if (da != 0)
                {
                    ta = Math.Abs(da / (feedcurrent / lenght_mm_cm_inc));
                }
                else
                {
                    ta = 0.0;
                }
                if (db != 0)
                {
                    tb = Math.Abs(db / (feedcurrent / lenght_mm_cm_inc));
                }
                else
                {
                    tb = 0.0;
                }
                if (dc != 0)
                {
                    tc = Math.Abs(dc / (feedcurrent / lenght_mm_cm_inc));
                }
                else
                {
                    tc = 0.0;
                }


                tmax = selectmax6val(tx, ty, tz, ta, tb, tc);

                dtot = Math.Sqrt(dx * dx + dy * dy + dz * dz);


                if (tmax <= 0.0)
                {
                    vel = feedcurrent;
                }
                else
                {
                    vel = dtot / tmax;
                }
            }

            return vel;
        }
        //----------------------------------------------------------------------------------------------------------------------//
        public double getStraightAcceleration(double lenght_mm_cm_inc,
            double xnew, double ynew, double znew, double anew, double bnew, double cnew,
            double xold, double yold, double zold, double aold, double bold, double cold
            )
        {
            double dx, dy, dz, da, db, dc;
            double tx, ty, tz, ta, tb, tc, tmax;
            double acc, dtot;
            bool cartesian_move = false;
            bool angular_move = false;
            acc = 0.0; // if a move to nowhere

            // Compute absolute travel distance for each axis:
            dx = Math.Abs(xnew - xold);
            dy = Math.Abs(ynew - yold);
            dz = Math.Abs(znew - zold);
            da = Math.Abs(anew - aold);
            db = Math.Abs(bnew - bold);
            dc = Math.Abs(cnew - cold);


            if (dx < tiny) dx = 0.0;
            if (dy < tiny) dy = 0.0;
            if (dz < tiny) dz = 0.0;
            if (da < tiny) da = 0.0;
            if (db < tiny) db = 0.0;
            if (dc < tiny) dc = 0.0;



            // Figure out what kind of move we're making.  This is used to determine
            // the units of vel/acc.
            if (dx <= 0.0 && dy <= 0.0 && dz <= 0.0)
            {
                cartesian_move = false;
            }
            else
            {
                cartesian_move = true;
            }
            if (da <= 0.0 && db <= 0.0 && dc <= 0.0)
            {
                angular_move = false;
            }
            else
            {
                angular_move = true;
            }

            // Pure linear move:
            if (cartesian_move && !angular_move)
            {


                if (dx != 0)
                {
                    tx = Math.Abs(dx / (Properties.Settings.Default.MaxFeedRateX / lenght_mm_cm_inc));
                }
                else
                {
                    tx = 0.0;
                }

                if (dy != 0)
                {
                    ty = Math.Abs(dy / (Properties.Settings.Default.MaxFeedRateY / lenght_mm_cm_inc));
                }
                else
                {
                    ty = 0.0;
                }

                if (dz != 0)
                {
                    tz = Math.Abs(dz / (Properties.Settings.Default.MaxFeedRateZ / lenght_mm_cm_inc));
                }
                else
                {
                    tz = 0.0;
                }

                tmax = selectmax3val(tx, ty, tz);

                dtot = Math.Sqrt(dx * dx + dy * dy + dz * dz);


                if (tmax > 0.0)
                {
                    acc = dtot / tmax;
                }


            }
            // Pure angular move:
            else if (!cartesian_move && angular_move)
            {

                if (da != 0)
                {
                    ta = Math.Abs(dx / (Properties.Settings.Default.MaxFeedRateA / lenght_mm_cm_inc));
                }
                else
                {
                    ta = 0.0;
                }

                if (db != 0)
                {
                    tb = Math.Abs(dy / (Properties.Settings.Default.MaxFeedRateB / lenght_mm_cm_inc));
                }
                else
                {
                    tb = 0.0;
                }

                if (dc != 0)
                {
                    tc = Math.Abs(dz / (Properties.Settings.Default.MaxFeedRateC / lenght_mm_cm_inc));
                }
                else
                {
                    tc = 0.0;
                }

                tmax = selectmax3val(ta, tb, tc);






                dtot = Math.Sqrt(da * da + db * db + dc * dc);
                if (tmax > 0.0)
                {
                    acc = dtot / tmax;
                }
            }
            // Combination angular and linear move:
            else if (cartesian_move && angular_move)
            {




                if (dx != 0)
                {
                    tx = Math.Abs(dx / (Properties.Settings.Default.MaxFeedRateX / lenght_mm_cm_inc));
                }
                else
                {
                    tx = 0.0;
                }

                if (dy != 0)
                {
                    ty = Math.Abs(dy / (Properties.Settings.Default.MaxFeedRateY / lenght_mm_cm_inc));
                }
                else
                {
                    ty = 0.0;
                }

                if (dz != 0)
                {
                    tz = Math.Abs(dz / (Properties.Settings.Default.MaxFeedRateZ / lenght_mm_cm_inc));
                }
                else
                {
                    tz = 0.0;
                }

                if (da != 0)
                {
                    ta = Math.Abs(dx / (Properties.Settings.Default.MaxFeedRateA / lenght_mm_cm_inc));
                }
                else
                {
                    ta = 0.0;
                }

                if (db != 0)
                {
                    tb = Math.Abs(dy / (Properties.Settings.Default.MaxFeedRateB / lenght_mm_cm_inc));
                }
                else
                {
                    tb = 0.0;
                }

                if (dc != 0)
                {
                    tc = Math.Abs(dz / (Properties.Settings.Default.MaxFeedRateC / lenght_mm_cm_inc));
                }
                else
                {
                    tc = 0.0;
                }
                tmax = selectmax6val(tx, ty, tz, ta, tb, tc);



                dtot = Math.Sqrt(dx * dx + dy * dy + dz * dz);


                if (tmax > 0.0)
                {
                    acc = dtot / tmax;
                }
            }

            return acc;
        }
        //----------------------------------------------------------------------------------------------------------------------//
        public bool getlimitaxis(double x, double y, double z, double a, double b, double c)
        {
            if ((x < Properties.Settings.Default.LimitXmin) || (x > Properties.Settings.Default.LimitXmax))
            {
                return true;
            }

            if ((y < Properties.Settings.Default.LimitYmin) || (y > Properties.Settings.Default.LimitYmax))
            {
                return true;
            }

            if ((z < Properties.Settings.Default.LimitZmin) || (z > Properties.Settings.Default.LimitZmax))
            {
                return true;
            }

            if ((a < Properties.Settings.Default.LimitAmin) || (a > Properties.Settings.Default.LimitAmax))
            {
                return true;
            }

            if ((b < Properties.Settings.Default.LimitBmin) || (b > Properties.Settings.Default.LimitBmax))
            {
                return true;
            }

            if ((c < Properties.Settings.Default.LimitCmin) || (c > Properties.Settings.Default.LimitCmax))
            {
                return true;
            }

            
            return false;
        }
        //----------------------------------------------------------------------------------------------------------------------//



















    }
}
